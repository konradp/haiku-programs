#include "MainWindow.h"
#include "messages.h"

#include "ScalesView.h"
#include "ChordsView.h"


#include <GroupLayoutBuilder.h>
#include <MenuBar.h>
#include <MenuItem.h>
#include <TabView.h>
#include <View.h>
#include <ControlLook.h>

#include <Rect.h>
#include <stdio.h>
//#include <TreeView.h>



// CONSTRUCTOR
MainWindow::	MainWindow(BRect rect)
	:	BWindow(rect, "GuitarChords window", B_DOCUMENT_WINDOW, B_ASYNCHRONOUS_CONTROLS)
{
	CreateViews();
}

// DESTRUCTOR
MainWindow::	~MainWindow()
{
}

void			//MESSAGE_RECEIVED
MainWindow::	MessageReceived(BMessage * msg)
{
	switch (msg->what)
	{
		// TODO: the following two are repeated
		case MSG_CHANGE_BASE:
		{	int32 basenote, type;
			if ( msg->FindInt32("note", &basenote) == B_OK && msg->FindInt32("type", &type) == B_OK ) {
				printf("Found base note %d and type %d \n", basenote, type);
				printf("%d \n", fTabView->Selection());
				if (  fTabView->Selection() == 0 )
					DrawScale(basenote, type);
				else
					DrawChord(basenote, type);
			}
			else printf("Couldn't find \n");
			break;
		}
		
		case MSG_CHANGE_TYPE:
		{	int32 basenote, type;
			if ( msg->FindInt32("note", &basenote) == B_OK && msg->FindInt32("type", &type) == B_OK ) {
				printf("Found base note %d and type %d \n", basenote, type);
				if (  fTabView->Selection() == 0 )
					DrawScale(basenote, type);
				else
					DrawChord(basenote, type);
			}
			else printf("Couldn't find \n");
			break;
		}
		
		case MSG_SETTINGS_WINDOW:
		{	BMenuItem* item = fMenuBar->FindItem("Look");
			if ( item->IsMarked() == false ) {
				item->SetMarked(true);
				fSettingsWindow->Show();	
			}
			else if ( item->IsMarked() == true) {
				printf("hello \n");
				item->SetMarked(false);
				// if is hidden=false then hide
				fSettingsWindow->Hide();	
			}
			else {	// Window was closed manually
				printf("Something's wrong \n");
			}
			break;
		}
		
		case MSG_COLOR1_CHANGED:
		{	printf("Main window: Color1 changed");
			break;
		}
			
			
		default:
		{	printf("MainWindow: Unknown MSG \n");
			break;
		}
		
	} //switch
}

void
MainWindow::	CreateViews()
{
	
	BWindow* controlWindow = new BWindow(BRect(500, 30, 700, 420), "Controls",
		B_FLOATING_WINDOW_LOOK, B_FLOATING_APP_WINDOW_FEEL,
		B_NOT_CLOSABLE | B_NOT_ZOOMABLE | B_NOT_RESIZABLE
			| B_ASYNCHRONOUS_CONTROLS | B_AUTO_UPDATE_SIZE_LIMITS);
	
	controlWindow->SetLayout(new BGroupLayout(B_VERTICAL));
	
	ScalesView* scalesView = new ScalesView();
	scalesView->SetTarget(this);
	ChordsView* chordsView = new ChordsView();
	chordsView->SetTarget(this);
	
	fTabView = new BTabView("tabView", B_WIDTH_AS_USUAL);
	fTabView->AddTab(scalesView);
	fTabView->AddTab(chordsView);
	controlWindow->AddChild(fTabView);
	
	controlWindow->Show();
	
	_MakeMenuBar();
	fFretboardView = new FretboardView(Bounds());
	//BView* view = new BView("blaa", 0);
	// LAYOUT
	//SetLayout(new BGroupLayout(B_HORIZONTAL, 0));
	//AddChild(fFretboardView);
	//AddChild(BGroupLayoutBuilder(B_HORIZONTAL, 0)
	//	.AddGlue()
	//	.Add(fFretboardView)
	//	.SetInsets(10, 0, 0, 0)
	//);
	
	fSettingsWindow = new SettingsWindow();
	fSettingsWindow->SetTarget(this);
	
	fSettingsWindow->Show();
	fSettingsWindow->Hide();
	
	AddChild(fMenuBar);
	
	AddChild(fFretboardView);
	
	
	//BRect rect(0.0f, 0.0f, 10.0f, 10.0f);
	//be_control_look->DrawArrowShape(view, rect, rect, view->HighColor(),
	//	0, 0, B_DARKEN_3_TINT);
}

void
MainWindow::	DrawChord(int basenote, int type)
{
	int major[7] = { 0, 0, 4, 0, 7, 0, 0 };
	int minor[7] = { 0, 0, 3, 0, 7, 0, 0 };
	int minorHarm[7] = { 0, 0, 3, 0, 7, 0, 0 };
	
	//int notes[7];
	
	int arr[7];
	if ( type == 0 )
		for ( int i = 0; i < 7; i++ )
			arr[i] = major[i];
	else if ( type == 1 )
		for ( int i = 0; i < 7; i++ )
			arr[i] = minor[i];
	else if ( type == 2 )
		for ( int i = 0; i < 7; i++ )
			arr[i] = minorHarm[i];

	int numberNotes = 7;
	for ( int i = 0; i < 7; i++ ) {
		*(arr+i) = ( *(arr+i) + basenote)%12;
	}
	
	fFretboardView->DrawChord(7, arr);
}

void
MainWindow::	DrawScale(int basenote, int type)
{
	int major[7] = { 0, 2, 4, 5, 7, 9, 11 };
	int minor[7] = { 0, 2, 3, 5, 7, 8, 10 };
	int minorHarm[7] = { 0, 2, 3, 5, 7, 8, 11 };
	
	int arr[7];
	if ( type == 0 )
		for ( int i = 0; i < 7; i++ )
			arr[i] = major[i];
	else if ( type == 1 )
		for ( int i = 0; i < 7; i++ )
			arr[i] = minor[i];
	else if ( type == 2 )
		for ( int i = 0; i < 7; i++ )
			arr[i] = minorHarm[i];

	int numberNotes = 7;
	for ( int i = 0; i < 7; i++ ) {
		*(arr+i) = ( *(arr+i) + basenote)%12;
	}
	
	fFretboardView->DrawScale(7, arr);
}

void
MainWindow::_MakeMenuBar()
{
	BMenuItem* menuItem;
	fMenuBar = new BMenuBar(BRect(0, 0, 0, 0), "menubar");
	
	// EXPORT MENU
	BMenu* exportMenu = new BMenu("Export to...");
	fMenuBar->AddItem(exportMenu);
		exportMenu->AddItem(menuItem = new BMenuItem("tab", new BMessage('savE')), 0);
		menuItem->SetShortcut('S', B_COMMAND_KEY);
		exportMenu->AddItem(menuItem = new BMenuItem("Save as...", new BMessage('savN')), 1);
		exportMenu->AddSeparatorItem();
	
	// SETTINGS MENU
	BMenu* settingsMenu = new BMenu("Settings");
	fMenuBar->AddItem(settingsMenu);
		settingsMenu->AddItem(menuItem = new BMenuItem("Look", new BMessage(MSG_SETTINGS_WINDOW) ), 0);
		settingsMenu->AddItem(menuItem = new BMenuItem("item", new BMessage('savN')), 1);
}



