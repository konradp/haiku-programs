#include "App.h"





App::	App(void)	//Constructor
	:	BApplication("application/x-vnd.Haiku-GuitarChords")
{
	// Main window
	MainWindow* fMainWindow = new MainWindow(BRect(30, 100, 705, 350));
	
	
	
	fMainWindow->Show();

}

App::	~App(void)	//destructor
{
}

//void
//App::MessageReceived(BMessage* message)
//{
//	BApplication::MessageReceived(message);
//}

bool
App::QuitRequested()
{
	return true;
}

int
main(void)
{
	App *app = new App();
	app->Run();
	delete app;
	return 0;
}
