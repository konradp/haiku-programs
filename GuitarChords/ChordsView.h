#ifndef ChordsView_H
#define ChordsView_H


#include <GroupView.h>

class BMenu;
class BMenuField;
class BMessageRunner;
class BMessenger;



class ChordsView : public BGroupView {
public:
					ChordsView();
	virtual			~ChordsView();
	virtual void	AttachedToWindow();
	void			MessageReceived(BMessage* message);

	void			SetTarget(BHandler* handler);

private:
	void			_AddBaseNoteMenu();
	void			_UpdateBaseNoteMenu(bool setInitialmenu);
	
	void			_AddScaleTypeMenu();
	void			_UpdateScaleTypeMenu(bool setInitialmenu);
	
	BMenu*			fBaseNoteMenu;
	BMenuField*		fBaseNoteMenuField;
	BMenu*			fScaleTypeMenu;
	BMenuField*		fScaleTypeMenuField;

private:
	int				fBaseNote;
	int				fScaleType;

								
private:
	BMessenger*		fMessenger;
	BMessageRunner*	fMessageRunner;

};


#endif // _H
