#ifndef MESSAGES_H
#define MESSAGES_H

#include <SupportDefs.h>

const uint32 MSG_CHANGE_BASE 		=	'baSe';
const uint32 MSG_CHANGE_TYPE 		=	'tyPe';
const uint32 MSG_SETTINGS_WINDOW	=	'stGs';
const uint32 MSG_COLOR1_CHANGED		=	'coL1';


#endif // MESSAGES_H
