#include "ScalesView.h"
#include "messages.h"

#include <Button.h>
#include <Menu.h>
#include <MenuField.h>
#include <MenuItem.h>
#include <MessageRunner.h>
#include <Messenger.h>
#include <PopUpMenu.h>
#include <stdio.h>
#include <String.h>



ScalesView::ScalesView()
	: BGroupView("Scale", B_VERTICAL),
	fMessenger(NULL),
	fMessageRunner(NULL),
	fBaseNoteMenu(NULL),
	fBaseNote(0),
	fScaleType(0)
{
	SetViewColor(ui_color(B_PANEL_BACKGROUND_COLOR));
	GroupLayout()->SetInsets(B_USE_WINDOW_SPACING);
}


ScalesView::~ScalesView()
{
	delete fMessenger;
	delete fMessageRunner;
}

void
ScalesView::AttachedToWindow()
{
	//fFontsizeSlider->SetTarget(this);
	//BButton* button = new BButton("bla", new BMessage(MSG_CHANGE_SCALE));
	//AddChild(button);
	//button->SetTarget(this);
	
	_AddBaseNoteMenu();
	_AddScaleTypeMenu();
}

void			//MESSAGE_RECEIVED
ScalesView::	MessageReceived(BMessage * msg)
{
	switch (msg->what)
	{
		// The following two are BAD. There should be one message for updating the scale
		// At the moment there are dirty repetitions
		case MSG_CHANGE_BASE:
		{	int32 placeholder;
			if ( msg->FindInt32("note", &placeholder) == B_OK ) {
				fBaseNote = placeholder;
				msg->AddInt32("type", fScaleType);
			}
			if ( msg->FindInt32("type", &placeholder) == B_OK ) {
				fScaleType = placeholder;
				msg->AddInt32("note", fBaseNote);
			}
		
			fMessenger->SendMessage(msg);
			break;
		}
		
		case MSG_CHANGE_TYPE:
		{	int32 placeholder;
			if ( msg->FindInt32("note", &placeholder) == B_OK ) {
				fBaseNote = placeholder;
				msg->AddInt32("type", fScaleType);
			}
			if ( msg->FindInt32("type", &placeholder) == B_OK ) {
				fScaleType = placeholder;
				msg->AddInt32("note", fBaseNote);
			}
		
			fMessenger->SendMessage(msg);
			break;
		}
			
		default:
		{	printf("ScalesView: Unknown MSG \n");
			break;
		}
		
	} //switch
}

void
ScalesView::SetTarget(BHandler* handler)
{
	delete fMessenger;
	fMessenger = new BMessenger(handler);
	//fDrawingModeMenu->SetTargetForItems(handler);
}

// The following is stolen from FontDemo
// TODO: reorganise this
void
ScalesView::_UpdateBaseNoteMenu(bool setInitialmenu)
{
	BMessage* itemMsg = NULL;
	BString notes[12] = { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };
	
	BMenuItem* menuitem;// = new BMenuItem(basescalemenu, familyMsg);
	for ( int i = 0; i < 12; i++ ) {
		itemMsg = new BMessage(MSG_CHANGE_BASE);
		itemMsg->AddInt32("note", i);
		menuitem = new BMenuItem(notes[i], itemMsg);
		fBaseNoteMenu->AddItem(menuitem);
		
		if ( setInitialmenu && i == fBaseNote) {
			menuitem->SetMarked(true);
			itemMsg = new BMessage(MSG_CHANGE_BASE);
			itemMsg->AddInt32("note", fBaseNote);
			itemMsg->AddInt32("type", fScaleType );
			fMessenger->SendMessage(itemMsg);
		} //if
			
	
	} //for
	
	fBaseNoteMenu->SetTargetForItems(this);
}

void
ScalesView::_UpdateScaleTypeMenu(bool setInitialmenu)
{
	BMessage* itemMsg = NULL;
	
	BString type[3] = { "major", "minor (natural)", "minor (harmonic)" };
	
	BMenuItem* menuitem;// = new BMenuItem(basescalemenu, familyMsg);
	for ( int i = 0; i < 3; i++ ) {
		itemMsg = new BMessage(MSG_CHANGE_TYPE);
		itemMsg->AddInt32("type", i);
		menuitem = new BMenuItem(type[i], itemMsg);
		fScaleTypeMenu->AddItem(menuitem);
		
		if ( setInitialmenu && i == fScaleType ) {
			menuitem->SetMarked(true);
			itemMsg = new BMessage(MSG_CHANGE_TYPE);
			itemMsg->AddInt32("type", fScaleType);
			itemMsg->AddInt32("note", fBaseNote);
			fMessenger->SendMessage(itemMsg);
		} //if
			
	
	} //for
	
	fScaleTypeMenu->SetTargetForItems(this);
}

void
ScalesView::_AddBaseNoteMenu()
{
	fBaseNoteMenu = new BPopUpMenu("<none>");

	fBaseNoteMenuField = new BMenuField("BaseNoteMenuField", "Base note:", fBaseNoteMenu);
	AddChild(fBaseNoteMenuField);

	_UpdateBaseNoteMenu(true);
}

void
ScalesView::_AddScaleTypeMenu()
{
	fScaleTypeMenu = new BPopUpMenu("<none>");

	fScaleTypeMenuField = new BMenuField("BaseNoteMenuField", "Type:", fScaleTypeMenu);
	AddChild(fScaleTypeMenuField);

	_UpdateScaleTypeMenu(true);
}
