#ifndef APP_H
#define APP_H

#include <Application.h>
#include "MainWindow.h"

class App : public BApplication
{
public:
	App();
	~App();
	
	//virtual	void	MessageReceived(BMessage* message);
	virtual	bool	QuitRequested();
private:
};

#endif
