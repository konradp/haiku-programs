/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <Window.h>
#include "FretboardView.h"
#include "SettingsWindow.h"

class MainWindow : public BWindow {
public:
					MainWindow(BRect frame);
					~MainWindow();
	void			MessageReceived(BMessage* message);

	void			CreateViews();

private:
	SettingsWindow*	fSettingsWindow;
	FretboardView*	fFretboardView;
	void			DrawChord(int basenote, int type);
	void			DrawScale(int basenote, int type);

	
	// MENU BAR
	void				_MakeMenuBar();
	BMenuBar*			fMenuBar;
	
private:
	BTabView*			fTabView;


};


#endif // MAINWINDOW_H
