#ifndef FretboardView_H
#define FretboardView_H

#include <View.h>
#include <ObjectList.h>
#include <PopUpMenu.h>


//#include "support/messages.h"

class FretboardView:	public BView {
public:
					//FretboardView(BList* samplesList);
					FretboardView(BRect rect);
	virtual void	Draw(BRect updateRect);
	virtual void	MouseDown(BPoint where);
	virtual	void	MouseMoved(BPoint where, uint32 code, const BMessage* message);
	

	
	void			InitMatrices();
	void			DrawChord(int numberNotes, int arr[]);
	void			DrawScale(int numberNotes, int arr[]);
	
	BList			fNodeList;
	BList			fTimeList[64];
	//BList*			fSamplesList;

private:
	void			fClearFretboard();
	void 			fCalculateChord(int numberNotes, int arr[]);
	void 			fCalculateScale(int numberNotes, int arr[]);


	int				fStringArray[6][16];
	float			fStringSpacing;
	float			fFretSpacing;
	int				fStringsNo;
	int				fFretsNo;
	bool			fFretboard[6][17];
	
	BPopUpMenu*		fNotePopUp;
	
	int				fRootNote;
	int				fThirdNote;
	int				fFifthNote;
	
	//int				fSampleWidth;
	//int				fWindowHorizShift;
};

#endif // FretboardView_H
