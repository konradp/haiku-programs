/* FretboardView:	This is the area where we draw out dots (notes on fretboard). We can draw max. 16 dots per string (15 frets + open string)
					For now this number is hard-coded. The note positions are numbered 0-15 and stored in a list.
					*/
#include <stdio.h>
#include "FretboardView.h"
#include <String.h>
#include <ControlLook.h>


FretboardView::	FretboardView(BRect rect)
	:	BView(rect, "Fretboard Frame", B_FOLLOW_ALL_SIDES, B_WILL_DRAW | B_FULL_UPDATE_ON_RESIZE),
		fStringSpacing(24),		// sample height 24 originally
		fStringsNo(6),		// number of strings
		fFretsNo(16),		// number of frets
		fNotePopUp(new BPopUpMenu("popup"))
{
	SetToolTip("blaa");
	
	InitMatrices();
	fClearFretboard();
}

//Draw
void
FretboardView::		Draw(BRect updateRect)
{
	pattern stripes = { {0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00} };
	//fStringSpacing = int(be_plain_font->Size()*2.33) | 1;	
	// BORDER
	BRect aRect(Bounds());
	aRect.InsetBy(20, 30);
	

	
	StrokeRect(aRect);

	//HORIZONTAL LINES
	for (int i = 0; i < fStringsNo; i++) {
		fStringSpacing = (i*aRect.Height())/5;
		BPoint vertShift(0, fStringSpacing);
		StrokeLine(aRect.LeftBottom() - vertShift, aRect.RightBottom() - vertShift);
	}
	
	BPoint horizShift(0, fStringSpacing);
	
	//VERTICAL LINES
	for (int i = 0; i < fFretsNo; i++) {
		fFretSpacing = (i*aRect.Width())/fFretsNo;
		BPoint horizShift(fFretSpacing, 0);
		// draw fret lines from right to left
		
		if ( i == 0 )
			StrokeLine(aRect.RightTop() - horizShift, aRect.RightBottom() - horizShift, stripes);
		else
			StrokeLine(aRect.RightTop() - horizShift, aRect.RightBottom() - horizShift);
		//BRect rect(aRect.RightTop() - horizShift, aRect.RightBottom() - horizShift);
		//rect.InsetBy(-0.5, 0);
		//FillRect(rect);
		
		BString string = BString();
		string << i;
		
		if ( i == 1 || i == 3 || i == 5 || i == 7 || i == 9 || i == 12 || i == 15 ) {
			
			if ( i != 1 ) {
				BRect dotRect(aRect.RightTop() - horizShift, aRect.RightBottom() - horizShift);
				dotRect.InsetBy(-0.5, 0);
				FillRect(dotRect);
			}
			//StrokeLine(aRect.RightTop() - horizShift, aRect.RightBottom() - horizShift);
			float width = StringWidth(string);
			
			DrawString(string, BPoint( aRect.RightBottom().x - width/2 , aRect.RightBottom().y+20 ) - horizShift);
		}
	}
	
	// DRAWING ALL POINTS
	for ( int i = 0; i < fStringsNo; i++ ) {
	fStringSpacing = (i*aRect.Height())/5;
		for ( int j = 0; j < fFretsNo; j++ ) {
		fFretSpacing = (j*aRect.Width())/fFretsNo;
			if ( fFretboard[i][j] == true ) {
				
				if ( fStringArray[i][j]%12 == fRootNote ) 
					SetHighColor(20,100,100);
				else if ( fStringArray[i][j]%12 == fThirdNote )
					SetHighColor(100,100,20);
				else if ( fStringArray[i][j]%12 == fFifthNote )
					SetHighColor(100,20,100);
				else
					SetHighColor(0,0,0);
				
				if ( j == 0 ) {
					SetPenSize(4);
					StrokeEllipse( BPoint(aRect.RightTop().x - fFretSpacing, aRect.RightBottom().y - fStringSpacing), 4, 4 );
					SetPenSize(1);
				}
				else
					FillEllipse( BPoint(aRect.RightTop().x - fFretSpacing, aRect.RightBottom().y - fStringSpacing), 5, 5 );
				}
					
				if ( fStringArray[i][j]%12 == fRootNote )
					StrokeEllipse( BPoint(aRect.RightTop().x - fFretSpacing, aRect.RightBottom().y - fStringSpacing), 9, 8 );
		}
	}
	
	BRect rect(0.0f, 0.0f, 10.0f, 10.0f);
	be_control_look->DrawArrowShape(this, rect, rect, this->HighColor(),
		0, 0, B_DARKEN_3_TINT);
}

// Mouse Down
void
FretboardView::		MouseDown(BPoint where)
{
	printf("FretboardView: MouseDown() \n");
}

void
FretboardView::MouseMoved(BPoint where, uint32 code, const BMessage* message)
{
	//printf("Moved \n");
	SetToolTip("");

	
	BRect aRect(Bounds());
	aRect.InsetBy(20, 30);
	
	BRect catchMouseRect(BRect(0,0,0,0));
	BString notes[12] = { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };



	BToolTip* toolTip = ToolTip();

	// TOOLTIP
	for ( int i = 0; i < fStringsNo; i++ ) {
		fStringSpacing = (i*aRect.Height())/5;
		for ( int j = 0; j < fFretsNo; j++ ) {
			fFretSpacing = (j*aRect.Width())/fFretsNo;

			//StrokeEllipse( BPoint(aRect.RightTop().x - fFretSpacing, aRect.RightBottom().y - fStringSpacing), 5, 5 );
			catchMouseRect.OffsetTo(	BPoint(aRect.RightTop().x - fFretSpacing, aRect.RightBottom().y - fStringSpacing) );
			catchMouseRect.InsetBy(-6,-6);
			if ( fFretboard[i][j] == true && catchMouseRect.Contains(where) ) {
				//StrokeRect(catchMouseRect);
				SetToolTip(notes[ fStringArray[i][j]%12 ]);
				ShowToolTip(toolTip);

			}	
			catchMouseRect.InsetBy(6,6);		

		}
	}
}

void
FretboardView::		fClearFretboard()
{
	for ( int i = 0; i < fStringsNo; i++ ) {
		for ( int j = 0; j < fFretsNo; j++ ) 
			fFretboard[i][j] = false;
	}
	Invalidate();
}

void
FretboardView::		DrawChord(int numberNotes, int arr[])
{
	fCalculateChord(numberNotes, arr);	
}

void
FretboardView::		DrawScale(int numberNotes, int arr[])
{
	fCalculateScale(numberNotes, arr);	
}

void
FretboardView::	InitMatrices()
{
	fStringsNo = 6;
	fFretsNo = 16;
	int stringTuning[6] =  { 28, 33, 38, 43, 47, 52 };
	// initialize stringTuning
	//for ( int a = 0; a < noStrings; a++ ) {
		
	//int stringArray[fStringsNo][fFretsNo];
	
	for ( int i = 0; i < fStringsNo; i++ ) {
		for ( int j = 0; j < fFretsNo; j++ ) {
			fStringArray[i][j] = stringTuning[i] + j;
		} //for
	} // for 
	
	for ( int i = 0; i < fStringsNo; i++ ) {
		printf("String: %d \n", i+1);
		printf("Notes: ");
		for ( int j = 0; j < fFretsNo; j++ ) {
			fStringArray[i][j] = stringTuning[i] + j;
			printf("%d ", fStringArray[i][j]);
		} //for
		printf("\n");
	} // for	
}

void
FretboardView::		fCalculateChord(int numberNotes, int arr[])
{
	printf("Calculating scale \n");
	fClearFretboard();
	fRootNote = arr[0];
	fThirdNote = arr[2];
	fFifthNote = arr[4];

	for ( int i = 0; i < fStringsNo; i++) {
		//printf("STRING \n");
		for ( int j = 0; j < fFretsNo; j++) {
			//printf("NOTE: %d ", fStringArray[i][j]);
			for ( int k = 0; k < numberNotes; k++) {
				if ( (fStringArray[i][j])%12 == arr[k] ) {
					//printf("TRUE %d and note is %d \n",  fStringArray[i][j], (fStringArray[i][j])%12 );
					
					fFretboard[i][j] = true;
				}
			} //for
		} //for
	} //for
}

void
FretboardView::		fCalculateScale(int numberNotes, int arr[])
{
	printf("Calculating scale \n");
	fClearFretboard();
	fRootNote = arr[0];
	fThirdNote = arr[2];
	fFifthNote = arr[4];
	
	for ( int i = 0; i < fStringsNo; i++) {
		//printf("STRING \n");
		for ( int j = 0; j < fFretsNo; j++) {
			//printf("NOTE: %d ", fStringArray[i][j]);
			for ( int k = 0; k < numberNotes; k++) {
				if ( (fStringArray[i][j])%12 == arr[k] ) {
					//printf("TRUE %d and note is %d \n",  fStringArray[i][j], (fStringArray[i][j])%12 );
					fFretboard[i][j] = true;
				}
			} //for
		} //for
	} //for
}



