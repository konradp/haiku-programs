/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef ScalesView_H
#define ScalesView_H


#include <GroupView.h>

class BMenu;
class BMenuField;
class BMessageRunner;
class BMessenger;



class ScalesView : public BGroupView {
public:
					ScalesView();
	virtual			~ScalesView();
	virtual void	AttachedToWindow();
	void			MessageReceived(BMessage* message);

	void			SetTarget(BHandler* handler);

private:
	void			_AddBaseNoteMenu();
	void			_UpdateBaseNoteMenu(bool setInitialmenu);
	
	void			_AddScaleTypeMenu();
	void			_UpdateScaleTypeMenu(bool setInitialmenu);
	
	BMenu*			fBaseNoteMenu;
	BMenuField*		fBaseNoteMenuField;
	BMenu*			fScaleTypeMenu;
	BMenuField*		fScaleTypeMenuField;

private:
	int				fBaseNote;
	int				fScaleType;

								
private:
	BMessenger*		fMessenger;
	BMessageRunner*	fMessageRunner;

};


#endif // _H
