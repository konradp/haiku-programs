#ifndef SettingsWindow_H
#define SettingsWindow_H

#include <Window.h>
#include <DirectWindow.h>
//#include "SettingsView.h"		//For control window

class BMessageRunner;
class BMessenger;

class SettingsWindow : public BWindow {
public:
					SettingsWindow();
	virtual			~SettingsWindow();
	virtual	void	MessageReceived(BMessage* message);
	void 			SetTarget(BHandler* handler);


								
private:
	//SettingsView*	fSettingsView;

	BMessenger*		fMessenger;
	BMessageRunner*	fMessageRunner;

};


#endif // _H
