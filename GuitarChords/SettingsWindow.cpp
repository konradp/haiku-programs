/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#include "SettingsWindow.h"
#include "messages.h"

#include <ColorControl.h>
#include <ControlLook.h>
#include <stdio.h>
#include <LayoutBuilder.h>
#include <GroupLayoutBuilder.h>
#include <ListView.h>
#include <TabView.h>
#include <MessageRunner.h>
#include <Messenger.h>

#include <ScrollView.h>


SettingsWindow::SettingsWindow()
	:
	BWindow(BRect(200,200,600,500), "Controls", B_FLOATING_WINDOW_LOOK, B_FLOATING_APP_WINDOW_FEEL,
		B_NOT_CLOSABLE | B_NOT_ZOOMABLE | B_ASYNCHRONOUS_CONTROLS)
{
	SetLayout(new BGroupLayout(B_VERTICAL));

	// COLORS
	//ONE
	BColorControl* colorControlOne = new BColorControl(BPoint(0, 0), B_CELLS_32x8, 7.0,
    "ColorControl1", new BMessage(MSG_COLOR1_CHANGED));
	colorControlOne->SetValue(0x20100100);
	colorControlOne->SetTarget(this);
	//TWO
	BColorControl* colorControlTwo = new BColorControl(BPoint(0, 60), B_CELLS_32x8, 7.0,
    "ColorControl2", new BMessage(MSG_COLOR1_CHANGED));
	colorControlTwo->SetValue(0x336698);
	// THREE
	BColorControl* colorControlThree = new BColorControl(BPoint(0, 120), B_CELLS_32x8, 7.0,
    "ColorControl3", new BMessage(MSG_COLOR1_CHANGED));
	colorControlThree->SetValue(0x336698);

	
	// TOP BACKGROUND VIEW
	BView* topView = new BView("topView", B_WILL_DRAW);
	BLayoutBuilder::Group<>(this, B_VERTICAL)
		.SetInsets(0, 0, 0, 0)
		.Add(colorControlOne)
		.Add(colorControlTwo)
		.Add(colorControlThree)
		.AddGlue()
		.End();
	//GetLayout()->AddView(topView);
}


SettingsWindow::~SettingsWindow()
{
}


void
SettingsWindow::MessageReceived(BMessage* msg)
{
	
	switch (msg->what) {

		default:
		{ printf("Settings Window: Unknown message\n");
		
		}
	}
}


void
SettingsWindow::SetTarget(BHandler* handler)
{
	delete fMessenger;
	fMessenger = new BMessenger(handler);
}
