#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <Window.h>
#include "BarView.h"

class MainWindow : public BWindow {
public:
		MainWindow(BRect frame);
		~MainWindow();
private:
	NBarView*	fBarView;
};


#endif // MAINWINDOW_H
