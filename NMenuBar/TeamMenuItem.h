#ifndef MYMENUITEM_H
#define MYMENUITEM_H

#include <MenuItem.h>
#include <View.h>

//class BMenuItem;
//class BarView;
class WindowMenu;

class TeamMenuItem : public BMenuItem {
public:
				TeamMenuItem(BList* team, char* name);
									
	virtual		~TeamMenuItem();	
	void		GetContentSize(float* width, float* height);
	BRect		LabelBounds();

	void		Draw();
	void		DrawContent();

	int			CountItems();
	bool		IsExpanded() const { return fExpanded; };
	void		ToggleExpandState();
	void		SetArrowDirection(int32 direction)
					{ fArrowDirection = direction; };
	BRect		ExpanderArrowBounds();
	bool		fEmpty;	
						
private:
	BList*		fTeam;
	void		_Init(BList* team, char* name);
	bool		fExpanded;
	int32		fArrowDirection;
	char*		fName;
};

#endif // _H
