#include "ChildMenuItem.h"
#include "WindowMenu.h"
#include "NMenuBar.h"

#include <MenuItem.h>
#include <View.h>
#include <ControlLook.h>
#include <stdio.h>

const float kArrowWidth = 16.0f;
const float kHPad = 10.0f;
const float kVPad = 2.0f;
const float kLabelOffset = 2.0f;

ChildMenuItem::ChildMenuItem(const char* label, BMessage* message)
	:
	BMenuItem(label, message)
{
	BFont font(be_plain_font);
	font_height fontHeight;
	font.GetHeight(&fontHeight);
	fLabelAscent = ceilf(fontHeight.ascent);
	fLabelDescent = ceilf(fontHeight.descent + fontHeight.leading);
}

ChildMenuItem::~ChildMenuItem()
{
	//delete fTeam;
}

void
ChildMenuItem::DrawContent()
{
	BMenu* menu = Menu();
	BPoint contentLocation = ContentLocation() + BPoint(kHPad, kVPad);

	// Draw item bounding box
	BRect frame(LabelBounds());
	menu->StrokeRect(frame);

	//if (fID >= 0) {
		menu->SetDrawingMode(B_OP_OVER);

		//float width = fBitmap->Bounds().Width();
		//if (width > 16)
			contentLocation.x -= 8;

		menu->MovePenTo(contentLocation);
		//menu->DrawBitmapAsync(fBitmap);

		//if (width > 16)
		//	contentLocation.x += 8;

		//contentLocation.x += kIconRect.Width() + kLabelOffset;
		contentLocation.x += kArrowWidth + kLabelOffset;
	//}
	contentLocation.y += fLabelAscent;

	menu->SetDrawingMode(B_OP_COPY);
	menu->MovePenTo(contentLocation);

	if (IsSelected())
		menu->SetHighColor(ui_color(B_MENU_SELECTED_ITEM_TEXT_COLOR));
	else
		menu->SetHighColor(ui_color(B_MENU_ITEM_TEXT_COLOR));

	menu->DrawString(Label());
}

BRect
ChildMenuItem::LabelBounds()
{
	BRect frame(Frame());
	//frame.left += kArrowWidth+3;
	frame.left += kArrowWidth;
	return frame;
}

// reimplemented from BTextControl?
void
ChildMenuItem::GetContentSize(float* width, float* height)
{
	BMenuItem::GetContentSize(width, height);
	//*height = 21;
	*height = 16;
	*width = *width + kArrowWidth;
}