#include "WindowMenu.h"
#include "BarView.h"
#include "App.h"
#include "ChildMenuItem.h"

#include <stdio.h>


WindowMenu::WindowMenu(const BList* team)
	:
	BMenu("WindowMenu"),
	fTeam(team)
{
	//printf("WindowMenu constructor with %d items\n", team->CountItems());
	fTeam = team;
	int32 teamCount = fTeam->CountItems();
	for (int i = 0; i < teamCount; i++ ) {
		AddItem((BMenuItem*)fTeam->ItemAt(i));
		//AddItem((ChildMenuItem*)fTeam->ItemAt(i));
	}
	//printf("WindowMenu: AItem count: %d \n", teamCount);
}

/*void
WindowMenu::AttachedToWindow()
{
	printf("WindowMenu: Attached to window \n");
	RemoveItems(0, CountItems(), true);

	int32 teamCount = fTeam->CountItems();
	printf("WindowMenu: team count: %d \n", teamCount);
	BMenu::AttachedToWindow();
}*/