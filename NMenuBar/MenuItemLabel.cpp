/*
 * When we choose to rename a menu item, this is the text field which appears in its place
 * This is essentially BTextControl, which takes less space, otherwise
 * the text field would be too big
 */
#include "MenuItemLabel.h"

#include <TextControl.h>

//	#pragma mark - MenuItemLabel constructor
MenuItemLabel::MenuItemLabel(BRect rect, const char* initialText, BMessage* message)
	:
	BTextControl(rect, "name", NULL, initialText, message, B_FOLLOW_LEFT, B_WILL_DRAW)
{
}

void
MenuItemLabel::GetPreferredSize(float* _width, float* _height)
{
	BTextControl::GetPreferredSize(_width, _height);
	//*_height = 16;
	*_height = 20;
	//*_width = *_width;// + kArrowWidth;
}