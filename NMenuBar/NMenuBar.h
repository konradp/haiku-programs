#ifndef NMENUBAR_H
#define NMENUBAR_H

#include <MenuBar.h>
#include <Locker.h>
#include <MenuItem.h>
#include <TextControl.h>

#include "MenuItemLabel.h"
#include "ChildMenuItem.h"


class TeamMenuItem;
class BarView;


class NMenuBar : public BMenuBar {
public:
					NMenuBar(BView* parentView);
					
	virtual	void	AttachedToWindow();			
	virtual	void	Draw(BRect update);
	void			MessageReceived(BMessage* message);
	virtual	void	MouseDown(BPoint where);


	void			BuildItems();
	TeamMenuItem*	TeamItemAtPoint(BPoint location,
					//	BMenuItem** _item = NULL);
						ChildMenuItem** _item = NULL);
	TeamMenuItem*	TeamItemAtIndex(int32 index);
	BMenuItem*		MenuItemAtPoint(BPoint location);
	//void			RenameItem(int32 id);				
	


private:
	// mouse actions: we clicked on the arrow. Expand a group/team
	void			AddNItem(int32 index);
	void			RenameNItem(int32 index);
	void			RemoveNItem(int32 index);
	bool			fTracking;
	TeamMenuItem*	fLastClickedTeam;
	BMenuItem*		fLastClickedItem;
	TeamMenuItem*	fLastHoveredTeam;
	BMenuItem*		fLastHoveredItem;
	//BTextControl*	fItemNameField;
	MenuItemLabel*	fItemNameField;
	bool			fLastClckItemExpState;
	void			_DoneArrowTracking(BPoint where);
	void			_TrackArrow(BPoint where, uint32);
	// we are dragging a team (expandable parent item) to another place
	void			_DoneTeamMoveTracking(BPoint where);
	void			_TrackTeamMove(BPoint where, uint32);
	// we are dragging an item (non=expandable child item) to another place
	void			_DoneItemMoveTracking(BPoint where);
	void			_TrackItemMove(BPoint where, uint32);
	// Right-click menus
	//void			DisplayItemMenu(BPoint where, int32 id) const;
	void			DisplayItemMenu(BPoint where, int32 id);
	void			DisplayTeamMenu(BPoint where, int32 id) const;



};


#endif // _H
