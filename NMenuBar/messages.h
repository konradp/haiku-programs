#ifndef MESSAGES_H
#define MESSAGES_H

//#include <SupportDefs.h>
const uint32 MENU_ADD_ITEM		=	'adIt';
const uint32 MENU_REMOVE_ITEM	=	'rmIt';
const uint32 MENU_ADD_GROUP		=	'adGr';
const uint32 MENU_RENAME_ITEM	=	'rnIt';
const uint32 ITEM_RENAMED		=	'Irnd';

#endif // MESSAGES_H
