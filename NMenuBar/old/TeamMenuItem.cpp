#include "TeamMenuItem.h"

#include <MenuItem.h>
#include <View.h>
#include <ControlLook.h>



TeamMenuItem::TeamMenuItem(char* name)
	:
	BMenuItem(new NSubmenu(name))
{
	//_Init(team, icon, name, signature, width, height);
}


TeamMenuItem::TeamMenuItem(float width, float height)
	:
	BMenuItem("", NULL)
{
	//_Init(NULL, NULL, strdup(""), strdup(""), width, height);

}


TeamMenuItem::~TeamMenuItem()
{
	//delete fTeam;
	//delete fIcon;
	//free(fSignature);
}

void
TeamMenuItem::GetContentSize(float* width, float* height)
{
	BMenuItem::GetContentSize(width, height);
	*height = 30;
}

void
TeamMenuItem::Draw()
{
	BRect frame = Frame();
	BMenu* menu = Menu();
	DrawContent();
}

void
TeamMenuItem::DrawContent()
{
	BMenu* menu = Menu();
	BRect frame = Frame();
	BRect rect(0.0f, 0.0f, 10, 10);
		
	menu->SetHighColor(ui_color(B_MENU_ITEM_TEXT_COLOR));
	menu->SetDrawingMode(B_OP_OVER);

	menu->StrokeRect(frame);
	rect.OffsetTo(BPoint(frame.left+2, ContentLocation().y));
	menu->StrokeRect(rect);
	be_control_look->DrawArrowShape(menu, rect, rect, menu->HighColor(),
		3, 0, B_DARKEN_3_TINT);

	menu->MovePenTo(ContentLocation() + BPoint(0, 10));
	menu->DrawString(Label());
}

