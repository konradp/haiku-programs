#ifndef TeamMenuItem_H
#define TeamMenuItem_H

#include <MenuItem.h>
#include <View.h>

class BMenuItem;

class TeamMenuItem : public BMenuItem {
public:
	TeamMenuItem(char* name);
	TeamMenuItem(float width = -1.0f,
									float height = -1.0f);
	virtual		~TeamMenuItem();
	
	void				GetContentSize(float* width, float* height);
	void				Draw();
	void				DrawContent();


	//BRect				ExpanderBounds() const;

								
private:

};


#endif // _H
