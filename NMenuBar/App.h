#ifndef APP_H
#define APP_H

#include <Application.h>
#include "MainWindow.h"

class App : public BApplication
{
public:
	App();
	~App();
	
	NBarView*				BarView() const { return fBarView; }
	
	//virtual	void	MessageReceived(BMessage* message);
	virtual	bool	QuitRequested();
private:
	NBarView*				fBarView;
};

#endif
