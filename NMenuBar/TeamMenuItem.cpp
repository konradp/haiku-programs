#include "TeamMenuItem.h"
#include "WindowMenu.h"
#include "NMenuBar.h"
#include "ChildMenuItem.h"

#include <MenuItem.h>
#include <View.h>
#include <ControlLook.h>
#include <stdio.h>

const float kArrowWidth = 16.0f;

TeamMenuItem::TeamMenuItem(BList* team, char* name)
	:
	BMenuItem(new WindowMenu(team)),
	fTeam(team),
	fExpanded(false)
{
	//fEmpty = true;
	fEmpty = true;
	_Init(team, name);
}

TeamMenuItem::~TeamMenuItem()
{
	//delete fTeam;
}

void
TeamMenuItem::GetContentSize(float* width, float* height)
{
	BMenuItem::GetContentSize(width, height);
	*height = kArrowWidth;
	*width = *width + kArrowWidth;
}

void
TeamMenuItem::Draw()
{
	DrawContent();
}

void
TeamMenuItem::DrawContent()
{
	BMenu* menu = Menu();
	menu->SetHighColor(ui_color(B_MENU_ITEM_TEXT_COLOR));
	menu->SetDrawingMode(B_OP_OVER);
	
	// Draw item bounding box
	BRect frame(Frame());
	menu->StrokeRect(frame);
	
	// Draw expander arrow and bounding box
	BRect arrowRect(0.0f, 0.0f, kArrowWidth, kArrowWidth);
	arrowRect.OffsetTo(BPoint(ContentLocation().x, ContentLocation().y));
	menu->StrokeRect(arrowRect);
	be_control_look->DrawArrowShape(menu, arrowRect, arrowRect, menu->LowColor(),
		fArrowDirection, 0, B_DARKEN_3_TINT);
	
	// Draw label
	menu->MovePenTo(ContentLocation() + BPoint(kArrowWidth+2, kArrowWidth-3));
	menu->SetHighColor(ui_color(B_MENU_ITEM_TEXT_COLOR));
	menu->DrawString(Label());
}

BRect
TeamMenuItem::LabelBounds()
{
	BRect frame(Frame());
	//frame.left += kArrowWidth+3;
	frame.left += kArrowWidth;
	return frame;
}

int
TeamMenuItem::CountItems()
{
	int count = 0;
	if (!fExpanded) {
		// Populate Menu() with the stuff from SubMenu().
		BMenu* menu = (static_cast<BMenu*>(Submenu()));
		if (menu != NULL) {
			//printf("TeamMenuItem: We found a submenu \n");
			count = menu->CountItems();
			//printf("TeamMenuItem: Items in the sub: %d \n", count);
			
		} //if
	} else if (fExpanded) {
		// Remove the goodies from the Menu() that should be in the SubMenu();
		WindowMenu* menu = static_cast<WindowMenu*>(Submenu());
		if (menu != NULL) {
			NMenuBar* parent = static_cast<NMenuBar*>(Menu());

			int32 childIndex = parent->IndexOf(this) + 1;			
			while (parent->SubmenuAt(childIndex) == NULL
				&& childIndex < parent->CountItems()) {
				count++;
				childIndex++;
			}
			return count;
		} //if
	} //else
	return count;
}

void
TeamMenuItem::ToggleExpandState()
{
	fExpanded = !fExpanded;
	fArrowDirection = fExpanded ? BControlLook::B_DOWN_ARROW
		: BControlLook::B_RIGHT_ARROW;

	if (fExpanded) {
		// Populate Menu() with the stuff from SubMenu().
		BMenu* submenu = (static_cast<BMenu*>(Submenu()));
		if (submenu != NULL) {
			printf("TeamMenuItem: We found a submenu \n");
			int count = submenu->CountItems();
			printf("TeamMenuItem: Items in the sub: %d \n", count);
			
			if (submenu->CountItems() > 0) {
				NMenuBar* parent = static_cast<NMenuBar*>(Menu());
				int myindex = parent->IndexOf(this) + 1;

				//BMenuItem* windowItem = NULL;
				ChildMenuItem* windowItem = NULL;
				int32 childIndex = 0;
				int32 totalChildren = submenu->CountItems();
				for (; childIndex < totalChildren; childIndex++) {
					//windowItem = static_cast<BMenuItem*>
					//	(submenu->RemoveItem((int32)0));
					windowItem = static_cast<ChildMenuItem*>
						(submenu->RemoveItem((int32)0));
					parent->AddItem(windowItem, myindex + childIndex);
					//parent->AddItem(windowItem, myindex);
				} //for
			} //if
		} //if
	} else {
		// Remove the goodies from the Menu() that should be in the SubMenu();
		WindowMenu* submenu = static_cast<WindowMenu*>(Submenu());
		if (submenu != NULL) {
			NMenuBar* parent = static_cast<NMenuBar*>(Menu());

			BMenuItem* windowItem = NULL;
			int32 childIndex = parent->IndexOf(this) + 1;
			while (parent->SubmenuAt(childIndex) == NULL
				&& childIndex < parent->CountItems()) {
				windowItem
					= static_cast<BMenuItem*>(parent->RemoveItem(childIndex));
				//submenu->AddItem(windowItem, submenu->CountItems());
				submenu->AddItem(windowItem);
				//windowItem->SetExpanded(false);
			} //while
		} //if
	} //else
}

BRect
TeamMenuItem::ExpanderArrowBounds()
{
	BRect frame(Frame());
	frame.right = frame.left + kArrowWidth;
	return frame;
}

// # pragma mark - Private methods

void
TeamMenuItem::_Init(BList* team, char* name)
{
	SetLabel(name);
	fExpanded = false;
	fArrowDirection = BControlLook::B_RIGHT_ARROW;
}
