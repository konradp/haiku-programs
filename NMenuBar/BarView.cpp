#include "BarView.h"
#include "WindowMenu.h"

#include "TeamMenuItem.h"
#include "ChildMenuItem.h"
#include "NMenuBar.h"

#include <Button.h>
#include <String.h>
#include <MenuItem.h>
#include <stdio.h>

NBarView::NBarView(BRect rect)
	:	BView(rect, "NBarView", B_FOLLOW_LEFT | B_FOLLOW_TOP, B_WILL_DRAW )
{
	printf("Barview: Constructor \n");
	NMenuBar* menubar = new NMenuBar(this);
		
	int j = 0;
	char childLabel[256];
	for ( int i = 0; i < 4; i++ ) {
		BList list;
		list.MakeEmpty();
		
		for ( int k = 0; k < 3; k++ ) {
			sprintf(childLabel, "Item %d", j);
			//BMenuItem *menuItem = new BMenuItem(childLabel, NULL);
			ChildMenuItem *menuItem = new ChildMenuItem(childLabel, NULL);
			list.AddItem(menuItem);
			j++;
		}
		char teamLabel[256];
		sprintf(teamLabel, "Group %d", i+1);
		
		TeamMenuItem* teamMenuItem3 = new TeamMenuItem(&list, teamLabel);
		teamMenuItem3->fEmpty = false;
		menubar->AddItem(teamMenuItem3);
	}
	AddChild(menubar);
	BButton* addParentButton = new BButton(NULL, NULL);
	AddChild(addParentButton);
}

NBarView::~NBarView()
{
}