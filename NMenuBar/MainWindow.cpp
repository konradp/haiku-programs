#include "MainWindow.h"
#include "TeamMenuItem.h"

#include <View.h>
#include <Rect.h>
#include <ControlLook.h>
#include <Menu.h>
#include <MenuBar.h>
#include <MenuItem.h>

#include "NMenuBar.h"

// CONSTRUCTOR
MainWindow::	MainWindow(BRect rect)
	:	BWindow(rect, "TreeView window", B_DOCUMENT_WINDOW, B_ASYNCHRONOUS_CONTROLS)
{
	fBarView = new NBarView(Bounds());
	AddChild(fBarView);		
}

// DESTRUCTOR
MainWindow::	~MainWindow()
{
}