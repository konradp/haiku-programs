#ifndef CHILDMENUITEM_H
#define CHILDMENUITEM_H

#include <MenuItem.h>
#include <View.h>

//class BMenuItem;
//class BarView;
class WindowMenu;

class ChildMenuItem : public BMenuItem {
public:
				ChildMenuItem(const char* label, BMessage* message);
									
	virtual		~ChildMenuItem();	
	void		GetContentSize(float* width, float* height);
	BRect		LabelBounds();

		

								
private:
	virtual void	DrawContent();
	float				fLabelAscent;
	float				fLabelDescent;
};


#endif // _H
