/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef MENUITEMLABEL_H
#define MENUITEMLABEL_H

#include <SupportDefs.h>
#include <TextControl.h>

class MenuItemLabel : public BTextControl {
public:
	MenuItemLabel(BRect rect, const char* initialText, BMessage* message);

	virtual	void	GetPreferredSize(float* _width,
						float* _height);					
private:

};


#endif // MENUITEMLABEL_H
