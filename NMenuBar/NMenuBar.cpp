#include "NMenuBar.h"
#include "messages.h"

#include <ControlLook.h> // for arrow expander
#include <private/shared/Thread.h> // for mouse tracking
#include <stdio.h> // for printf
#include <PopUpMenu.h>
#include <TextControl.h>
#include <SupportDefs.h>


#include "App.h"
#include "ChildMenuItem.h"
#include "TeamMenuItem.h"

const float khalfItemWidth = 15.0f;

//	#pragma mark - NMenuBar
NMenuBar::NMenuBar(BView* parentView)
	:
	BMenuBar(BRect(0, 0, 0, 0), "NMenuBar", B_FOLLOW_NONE, B_ITEMS_IN_COLUMN),
	fTracking(false),
	fLastClickedTeam(NULL),
	fLastClickedItem(NULL),
	fLastHoveredTeam(NULL),
	fItemNameField(NULL),
	fLastClckItemExpState(false)

{
	printf("MenuBar: Constructor \n");
	SetItemMargins(0.0f, 0.0f, 0.0f, 0.0f);
	SetFont(be_plain_font);
	BuildItems();
}

void
NMenuBar::AttachedToWindow()
{
	printf("MenuBar: Attached to window \n");
	BMenuBar::AttachedToWindow();
}

void
NMenuBar::BuildItems()
{
	//printf("MenuBar: BuildItems \n");
	int32 itemCount = CountItems();
	BList itemList(itemCount);
}

// Message Received
void
NMenuBar::MessageReceived(BMessage * msg)
{
	switch (msg->what) {
		// ITEM MESSAGES
		case MENU_ADD_ITEM: {
			int32 index;
			if (msg->FindInt32("index", &index) == B_OK) {
				AddNItem(index);
			}
			break;
		}
		
		case MENU_RENAME_ITEM: {
			int32 index;
			printf("Renaming \n");
			if (msg->FindInt32("index", &index) == B_OK) {
				printf("Found index \n");
				RenameNItem(index);
			}
			break;
		}
		
		case MENU_REMOVE_ITEM: {
			int32 index;
			if (msg->FindInt32("index", &index) == B_OK) {
				RemoveNItem(index);
			}
				
			break;
		}
		
		// GROUP MESSAGES
		case MENU_ADD_GROUP: {
			int32 index;
			if (msg->FindInt32("index", &index) == B_OK) {
				printf("Adding group at %d \n", index);
				BList emptyList;
				//BMenuItem* emptyItem = new BMenuItem("empty", NULL);
				ChildMenuItem* emptyItem = new ChildMenuItem("empty", NULL);
				emptyList.AddItem(emptyItem);
				TeamMenuItem* emptyGroup = new TeamMenuItem(&emptyList, "unnamed");
				AddItem(emptyGroup, index);
				emptyGroup->ToggleExpandState();
			}
			break;
		}
		
		// ITEM action MESSAGES
		case ITEM_RENAMED: {
			printf("Renamed \n");
			int32 index;
			BMenuItem* menuItem;
			if ( (msg->FindInt32("index", &index) == B_OK) && fItemNameField) {
				menuItem = (BMenuItem*)ItemAt(index);
				menuItem->SetLabel(fItemNameField->Text());
				RemoveChild(fItemNameField);
				fItemNameField = NULL;
			}
			break;
		}
		
		default: {
			BMenuBar::MessageReceived(msg);
			break;
		}	
	}
}

void
NMenuBar::MouseDown(BPoint where)
{
	printf("Mouse clicked \n");
	
	if (fItemNameField) {
		printf("We are editing a name \n");
		BMessage* modMessage;
		modMessage = fItemNameField->ModificationMessage();
		fItemNameField->Invoke(modMessage);
		return;
	}
	
	//BMenuItem* menuItem;
	ChildMenuItem* menuItem;
	TeamMenuItem* teamItem = TeamItemAtPoint(where, &menuItem);
	BMessage* message = Window()->CurrentMessage();
	
	if ( message && teamItem ) {
		fLastClickedTeam = teamItem;
		bool isTeam = ( (BMenuItem*)teamItem == (BMenuItem*)menuItem )? true : false;
		uint32 buttons;
		message->FindInt32("buttons", (int32*)&buttons);	
		
		// RIGHT-CLICK: display TEAM or ITEM menu
		if ((buttons & B_SECONDARY_MOUSE_BUTTON) != 0) {
			(isTeam) ? DisplayTeamMenu(where, IndexOf(teamItem))
				: DisplayItemMenu(where, IndexOf(menuItem));

		// LEFT-CLICK
		} else if (buttons) {
			// ARROW clicked: expand subitems
			if (teamItem->ExpanderArrowBounds().Contains(where)) {	
				MouseDownThread<NMenuBar>::TrackMouse(this,
					&NMenuBar::_DoneArrowTracking, &NMenuBar::_TrackArrow);
			// ARROW NOT clicked: dragging item	
			} else {	
				fTracking = true;	
				fLastHoveredTeam = teamItem;
				
				if ( !isTeam ) {
					// CHILD ITEM clicked
					fLastClickedItem = menuItem;
					MouseDownThread<NMenuBar>::TrackMouse(this,
						&NMenuBar::_DoneItemMoveTracking, &NMenuBar::_TrackItemMove);
				} else if ( isTeam && !(teamItem->fEmpty) ) {
					// TEAM ITEM clicked
					if ( fLastClckItemExpState = teamItem->IsExpanded() )
						teamItem->ToggleExpandState();
					MouseDownThread<NMenuBar>::TrackMouse(this,
						&NMenuBar::_DoneTeamMoveTracking, &NMenuBar::_TrackTeamMove);	
				} 
				Invalidate(fLastHoveredTeam->Frame());
			} //else
			return;	// absorb the message
		} //else if
		

	}
}

/*!	Returns the team menu item that belongs to the item under the
	specified point.
	If _item is given, it will return the exact menu item under
	that point (which might be a window item when the expander is on).
*/
TeamMenuItem*
//NMenuBar::TeamItemAtPoint(BPoint point, BMenuItem** _item)
NMenuBar::TeamItemAtPoint(BPoint point, ChildMenuItem** _item)
{
	point.x = 0;
	TeamMenuItem* lastApp = NULL;
	int32 count = CountItems();
	
	for (int32 i = 0; i < count; i++) {
		//BMenuItem* item = (BMenuItem*)ItemAt(i);
		ChildMenuItem* item = (ChildMenuItem*)ItemAt(i);

		if (dynamic_cast<TeamMenuItem*>(item) != NULL)
			lastApp = (TeamMenuItem*)item;
		
		if (item && item->Frame().Contains(point)) {
			if (_item != NULL)
				//*_item = (BMenuItem*)item;
				*_item = item;
			return lastApp;
		}
	}
	// no item found
	if (_item != NULL)
		*_item = NULL;
		
	return NULL;
}

/*!	Returns the team menu item that belongs to the item under the
	specified point.
	If _item is given, it will return the exact menu item under
	that point (which might be a window item when the expander is on).
*/
TeamMenuItem*
//NMenuBar::TeamItemAtPoint(BPoint point, BMenuItem** _item)
NMenuBar::TeamItemAtIndex(int32 index)
{
	TeamMenuItem* lastTeam = NULL;
	int32 count = CountItems();
	
	for (int32 i = 0; i < count; i++) {
		//BMenuItem* item = (BMenuItem*)ItemAt(i);
		ChildMenuItem* item = (ChildMenuItem*)ItemAt(i);
		
		if (dynamic_cast<TeamMenuItem*>(item) != NULL)
			lastTeam = (TeamMenuItem*)item;
		
		if ( item == (ChildMenuItem*)ItemAt(index) ) {
			return lastTeam;
		}
			
			
	}

}

BMenuItem*
NMenuBar::MenuItemAtPoint(BPoint point)
{
	BMenuItem* lastApp = NULL;
	int32 count = CountItems();
	
	for (int32 i = 0; i < count; i++) {
		BMenuItem* item = ItemAt(i);

		if (dynamic_cast<BMenuItem*>(item) != NULL)
			lastApp = (BMenuItem*)item;
		if (item && item->Frame().Contains(point))
			return lastApp;
	}	
	return NULL;
}


void
NMenuBar::Draw(BRect updateRect)
{

	BMenu::Draw(updateRect);
	if (fTracking && fLastHoveredTeam) {
		SetPenSize(4);
		StrokeRect(fLastHoveredTeam->Frame());
		SetPenSize(1);
	}
}


//	#pragma mark - Mouse tracking
void
NMenuBar::_DoneArrowTracking(BPoint point)
{
	TeamMenuItem* lastItem = dynamic_cast<TeamMenuItem*>(fLastClickedTeam);

	if (!lastItem->ExpanderArrowBounds().Contains(point))
		return;

	lastItem->ToggleExpandState();
	lastItem->SetArrowDirection(lastItem->IsExpanded()
		? BControlLook::B_DOWN_ARROW
		: BControlLook::B_RIGHT_ARROW);

	Invalidate(lastItem->ExpanderArrowBounds());
}


void
NMenuBar::_TrackArrow(BPoint point, uint32)
{
	TeamMenuItem* lastItem = dynamic_cast<TeamMenuItem*>(fLastClickedTeam);

	if (lastItem->ExpanderArrowBounds().Contains(point))
		lastItem->SetArrowDirection(BControlLook::B_RIGHT_DOWN_ARROW);
	else {
		lastItem->SetArrowDirection(lastItem->IsExpanded()
			? BControlLook::B_DOWN_ARROW
			: BControlLook::B_RIGHT_ARROW);
	}

	Invalidate(lastItem->ExpanderArrowBounds());
}


void
NMenuBar::_DoneTeamMoveTracking(BPoint point)
{
	if ( fLastClckItemExpState )
		fLastClickedTeam->ToggleExpandState();
	fTracking = false;
	Invalidate(Frame());
}


void
NMenuBar::_TrackTeamMove(BPoint where, uint32)
{
	//BMenuItem* menuItem;
	ChildMenuItem* menuItem;
	TeamMenuItem* hoveredTeam = TeamItemAtPoint(where, &menuItem);
	
	if (fLastClickedTeam && hoveredTeam && (fLastHoveredTeam != hoveredTeam)) {
		int from_index = IndexOf(fLastHoveredTeam);
		int hoveredItem_index = IndexOf(hoveredTeam);

		//printf("INDEXES: %d %d\n", from_index, hoveredItem_index);
		//printf("MenuBar: Hovering over team: %d \n", hoveredItem_index);
		
		TeamMenuItem* nextItem;
		//BMenuItem* dummyItem;
		ChildMenuItem* dummyItem;
		if ( hoveredItem_index > from_index )
			nextItem = TeamItemAtPoint(where+BPoint(0,khalfItemWidth), &dummyItem);
		if ( hoveredItem_index < from_index )
			nextItem = TeamItemAtPoint(where-BPoint(0,khalfItemWidth), &dummyItem);
		
		if ( nextItem != hoveredTeam) {
			bool hoveredItemState = false;
			if ( hoveredTeam->IsExpanded() ) {
				hoveredItemState = true;
				hoveredTeam->ToggleExpandState();
			}

			// Something wrong about this, why do the items appear to switch
			// before the rectangle is updated?
			// Check Draw() method
			RemoveItem(fLastClickedTeam);
			SetPenSize(4);
			StrokeRect(fLastHoveredTeam->Frame());
			SetPenSize(1);
			AddItem(fLastClickedTeam, hoveredItem_index);
			
			if ( hoveredItemState == true )
				hoveredTeam->ToggleExpandState();
			fLastHoveredTeam = hoveredTeam;			
		}
	}//if
}

void
NMenuBar::_DoneItemMoveTracking(BPoint point)
{
	if ( fLastClckItemExpState )
		fLastClickedTeam->ToggleExpandState();
	fTracking = false;
	Invalidate(Frame());
}

void
NMenuBar::_TrackItemMove(BPoint where, uint32)
{
	//BMenuItem* hoveredItem;
	ChildMenuItem* hoveredItem;
	TeamMenuItem* teamItem = TeamItemAtPoint(where, &hoveredItem);
	
	if ( fLastClickedItem && teamItem && hoveredItem && (hoveredItem != fLastHoveredItem) 
	&& ((BMenuItem*)teamItem == (BMenuItem*)fLastClickedTeam) && ((BMenuItem*)hoveredItem != (BMenuItem*)teamItem) ) {
		int from_index = IndexOf(fLastHoveredItem);
		int hoveredItem_index = IndexOf(hoveredItem);
		
		TeamMenuItem* nextTeam;
		//BMenuItem* nextItem;
		ChildMenuItem* nextItem;
		if ( hoveredItem_index > from_index )
			nextTeam = TeamItemAtPoint(where+BPoint(0,khalfItemWidth), &nextItem);
		else if ( hoveredItem_index < from_index )
			nextTeam = TeamItemAtPoint(where-BPoint(0,khalfItemWidth), &nextItem);
		
		if ( (hoveredItem != fLastHoveredItem) ) {
			RemoveItem(fLastClickedItem);
			AddItem(fLastClickedItem, hoveredItem_index);
		}
	}//if
	fLastHoveredItem = hoveredItem;
	fLastHoveredTeam = teamItem;
}

void
//NMenuBar::DisplayItemMenu(BPoint where, int32 id) const
NMenuBar::DisplayItemMenu(BPoint where, int32 id)
{	
	ChildMenuItem* menuItem;
	TeamMenuItem* teamItem = TeamItemAtPoint(where, &menuItem);
	
	BPopUpMenu* itemMenu = new BPopUpMenu("itemMenu", false, false);
	BMessage* message = NULL;
	BMenuItem* item = new BMenuItem("Empty item", message);
	int32 index = (int32) id;
	
	// ADD BUTTON
	message = new BMessage(MENU_ADD_ITEM);
	message->AddInt32("index", index);
	item = new BMenuItem(("Add item"), message);
	item->SetTarget(this);
	itemMenu->AddItem(item);
	
	// RENAME BUTTON
	message = new BMessage(MENU_RENAME_ITEM);
	message->AddInt32("index", index);
	item = new BMenuItem(("Rename"), message);
	item->SetTarget(this);
	itemMenu->AddItem(item);
	
	//FILL MENU
	BMenu* fillMenu = new BMenu("Fill...");
		// 2
		message = new BMessage('abcd');
		message->AddInt32("index", index);
		message->AddInt32("fillNumber", 2);
		item = new BMenuItem(("2"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 4
		message = new BMessage('abcd');
		message->AddInt32("index", index);
		message->AddInt32("fillNumber", 4);
		item = new BMenuItem(("4"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 8
		message = new BMessage('abcd');
		message->AddInt32("index", index);
		message->AddInt32("fillNumber", 8);
		item = new BMenuItem(("8"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 16
		message = new BMessage('abcd');
		message->AddInt32("index", index);
		message->AddInt32("fillNumber", 16);
		item = new BMenuItem(("16"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 32
		message = new BMessage('abcd');
		message->AddInt32("index", index);
		message->AddInt32("fillNumber", 32);
		item = new BMenuItem(("32"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 64
		message = new BMessage('abcd');
		message->AddInt32("index", index);
		message->AddInt32("fillNumber", 64);
		item = new BMenuItem(("64"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
	itemMenu->AddItem(fillMenu);
	// END FILL MENu
	
	// CLEAR BUTTON
	message = new BMessage('abcd');
	message->AddInt32("index", index);
	item = new BMenuItem(("Clear"), message);
	item->SetTarget(this);
	itemMenu->AddItem(item);
	
	itemMenu->AddSeparatorItem();
	// REMOVE ITEM BUTTON
	message = new BMessage(MENU_REMOVE_ITEM);
	message->AddInt32("index", index);
	item = new BMenuItem(("Remove item"), message);
	item->SetTarget(this);
	if (teamItem->fEmpty) item->SetEnabled(false);
	itemMenu->AddItem(item);
	
	// MENU methods
	where = ConvertToScreen(where);
	BRect mouseRect(where, where);
	mouseRect.InsetBy(-4.0, -4.0);
	itemMenu->Go(where, true, false, mouseRect, true);
}

void
NMenuBar::DisplayTeamMenu(BPoint where, int32 id) const
{
	printf("Team clicked: %lu \n", (int32) id);
	
	BPopUpMenu* teamMenu = new BPopUpMenu("TeamMenu", false, false);
	BMessage* message = NULL;
	BMenuItem* item = new BMenuItem("Empty item", message);
	int32 index = (int32) id;	

	// ADD GROUP BUTTON
	message = new BMessage(MENU_ADD_GROUP);
	message->AddInt32("index", index);
	item = new BMenuItem(("Add group"), message);
	item->SetTarget(this);
	teamMenu->AddItem(item);
	
	// RENAME BUTTON
	message = new BMessage(MENU_RENAME_ITEM);
	message->AddInt32("index", index);
	item = new BMenuItem(("Rename"), message);
	item->SetTarget(this);
	teamMenu->AddItem(item);
	
	teamMenu->AddSeparatorItem();
	// REMOVE ITEMS BUTTON
	message = new BMessage('abcd');
	message->AddInt32("index", index);
	item = new BMenuItem(("Remove all items"), message);
	item->SetTarget(this);
	teamMenu->AddItem(item);
	
	// REMOVE GROUP BUTTON
	message = new BMessage('abcd');
	message->AddInt32("index", index);
	item = new BMenuItem(("Remove group"), message);
	item->SetTarget(this);
	teamMenu->AddItem(item);
	
	// MENU methods
	where = ConvertToScreen(where);
	BRect mouseRect(where, where);
	mouseRect.InsetBy(-4.0, -4.0);
	teamMenu->Go(where, true, false, mouseRect, true);
	
}

void
NMenuBar::RenameNItem(int32 index)
{
		BMenuItem* menuItem;

		menuItem = (BMenuItem*)ItemAt(index);
		ChildMenuItem* childItem = dynamic_cast<ChildMenuItem*>(menuItem);
		TeamMenuItem* teamItem = dynamic_cast<TeamMenuItem*>(menuItem);
		
		//if (menuItem) printf("We have menu item \n");
		//if (childItem) printf("We have child item \n");
		if ( menuItem && childItem && !fItemNameField ) {
			BMessage* message;
			message = new BMessage(ITEM_RENAMED);
			message->AddInt32("index", index);
			fItemNameField = new MenuItemLabel(childItem->LabelBounds(),
			childItem->Label(), message);
			AddChild(fItemNameField);
			fItemNameField->SetTarget(this);
			fItemNameField->MakeFocus();
		}
		else if ( menuItem && teamItem && !fItemNameField ) {
			BMessage* message;
			message = new BMessage(ITEM_RENAMED);
			message->AddInt32("index", index);
			fItemNameField = new MenuItemLabel(teamItem->LabelBounds(),
			teamItem->Label(), message);
			AddChild(fItemNameField);
			fItemNameField->SetTarget(this);
			fItemNameField->MakeFocus();
			
		}	
}

void
NMenuBar::RemoveNItem(int32 index)
{
	BMenuItem* item = ItemAt(index);
	
	TeamMenuItem* teamItem = TeamItemAtIndex(index);
	if (teamItem && item) {
		printf("LOL: %d \n", teamItem->CountItems());
		if ( teamItem->CountItems() == 1 ) {
			teamItem->fEmpty = true;
			ChildMenuItem* emptyItem = new ChildMenuItem("empty", NULL);
			AddItem(emptyItem, index);
		}
	}
	RemoveItem(item);
}

void
NMenuBar::AddNItem(int32 index)
{
	TeamMenuItem* teamItem = TeamItemAtIndex(index);
	BMenuItem* item = ItemAt(index);
	if (teamItem->fEmpty && item ) {
		printf("It's empty \n");
		RemoveItem(item);	
	}
	//BMenuItem* emptyItem = new BMenuItem("unnamed", NULL);
	ChildMenuItem* emptyItem = new ChildMenuItem("unnamed", NULL);
	//TeamMenuItem(TeamMenuItem*) ItemAt(index)
	
	AddItem(emptyItem, index);
	RenameNItem(index);
}

/*
	// EXPAND team item if hovered long enough
	if ( teamItem && (teamItem == hoveredItem) && !(teamItem->fEmpty) ) {
		if ( hoveredItem != fLastHoveredTeam ) {
			fLastHoveredTeam = teamItem;
			fLastHoverTime = system_time();
		}
		if ( (system_time() > fLastHoverTime + 1500000)
			&& (teamItem != fCurrentParentTeam) ) {
			printf("System time: %llu %llu\n", system_time(), fLastHoverTime);
			teamItem->ToggleExpandState();
			fLastHoverTime = system_time();
		}
		//return;
	}
	*/