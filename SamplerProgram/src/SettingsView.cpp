#include "SettingsView.h"
#include "support/messages.h"

#include <Box.h>
#include <GroupLayoutBuilder.h>
#include <LayoutBuilder.h>
#include <Slider.h>

//#include <cmath>  //I thought we needed it for pow
#include <stdio.h>	//For debug printf messages

SettingsView::		SettingsView()		//Constructor
	:				BView("Settings", B_FOLLOW_ALL),
					fNodeSizeSlider(NULL)
{
	SetLayout(new BGroupLayout(B_VERTICAL));
	// SLIDER: RESOLUTION
	fNodeSizeSlider = new BSlider(NULL, "Fontsize", "Size: 8", NULL, 1, 6);
	fNodeSizeSlider->SetModificationMessage(new BMessage(RESOLUTION_MSG));	//message sent when adjusted
	fNodeSizeSlider->SetValue(3);	//initial slider value
	//fNodeSizeSlider->SetTarget(this);	//Crucial for messaging
	
	// SLIDER: TEMPO
	fTempoSlider = new BSlider(NULL, "Tempo", "BPM: 120", NULL, 40, 240);
	fTempoSlider->SetModificationMessage(new BMessage(MSG_TEMPO));	//message sent when adjusted
	fTempoSlider->SetValue(120);	//initial slider value
	fTempoSlider->SetTarget(this);	//Crucial for messaging

	//PLAY BUTTON
	fPlayButton = new BButton(NULL, "button", "PlaySample", new BMessage(MSG_PLAY_OLD));
	fPlayButton->SetTarget(this);
	
	BLayoutBuilder::Group<>(this, B_VERTICAL)
		.SetInsets(B_USE_SMALL_SPACING)
		.Add(fNodeSizeSlider)
		.Add(fTempoSlider)
		.Add(fPlayButton)
		.End();

}


SettingsView::~SettingsView()
{
}


int32
SettingsView::		GetResolution()
{
	printf("SettingsView: GetResolution \n");
			char buff[256];
			int32 value;	value = static_cast<int32>(fNodeSizeSlider->Value());
			printf("SettingsView: Resize received. Resolution: %lu\n", (int32) pow(2, value));
			sprintf(buff, "Size: %.0f", pow(2, value));
			fNodeSizeSlider->SetLabel(buff);
			return value;
}


int32
SettingsView::		GetTempo()
{
	printf("SettingsView: GetTempo \n");
			char buff[256];
			int32 value;
			value = static_cast<int32>(fTempoSlider->Value());
			printf("SettingsView: Tempo received. Tempo: %lu\n", (int32) pow(2, value));
			sprintf(buff, "Tempo: %lu", (int32) value);
			fTempoSlider->SetLabel(buff);
			return value;
}

