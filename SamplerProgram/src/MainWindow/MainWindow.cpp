// TODO: Add keyboard shortcuts for the menu

#include "MainWindow.h"
#include "media_nodes/MediaClock.h"
#include "media_nodes/MidiIn.h"
#include "support/messages.h"
#include "support/PlaybackState.h"

#include <stdio.h>
#include <Alert.h>
#include <GroupLayoutBuilder.h>
#include <LayoutBuilder.h>	//for Midi Mixer
#include <Menu.h>
#include <MenuBar.h>
#include <MenuItem.h>
#include <ScrollView.h>
#include <StorageKit.h>	//for be_app

#include <media/MediaTheme.h>

// CONSTRUCTOR
MainWindow::	MainWindow(BRect rect)
	:	BWindow(rect, "NFSampler", B_DOCUMENT_WINDOW, B_ASYNCHRONOUS_CONTROLS),
		fPlaybackState(0),
		fClockRunning(false),
		fRealResolution(8),		// real no. of lines 2^3=8
		fSamplesNumber(8),
		fTickPosition(0),
		fTick(0),
		midi_in(NULL),
		midi_out(NULL),
		fNodeManager(NULL)
{
	// MIDI
	midi_in = new MidiIn("SamplerMidiIn");
	midi_in->Register();
	midi_out = new BMidiLocalProducer("SamplerMidiOut");
	midi_out->Register();
	
	fSamplesList.MakeEmpty();
	for ( int i = 0; i < fSamplesNumber; i++ )
	{
		fSamplesList.AddItem(new SampleTrack());
	}
	
	CreateViews();
}

// DESTRUCTOR
MainWindow::	~MainWindow()
{
	printf("MainWin: Quitting \n");
	midi_in->Release();
	printf("MainWin: midi_in released \n");
	midi_out->Release();
	printf("MainWin: midi_out released \n");
}

void			//CREATE_VIEWS
MainWindow::	CreateViews()
{
	_MakeMenuBar();
	// VIEWS
	fPatternView = new PatternView(&fSamplesList);
	fPatternView->SetTarget(this);
	BScrollView* fScrollView;
	fScrollView = new BScrollView(NULL, fPatternView, 0, false, true);
	fTransportView = new TransportView();
	fTransportView->SetTarget(this);
	fTickView = new TickView(BRect(0,0,10,10));
	
	// LAYOUT
	SetLayout(new BGroupLayout(B_HORIZONTAL));
	AddChild(BGroupLayoutBuilder(B_VERTICAL, 0)
		.Add(BGroupLayoutBuilder(B_HORIZONTAL, 0)
			.Add(fMenuBar)
			.Add(fTransportView))
		.Add(BGroupLayoutBuilder(B_HORIZONTAL, 0)
			.Add(fTickView))
		.Add(BGroupLayoutBuilder(B_VERTICAL, 0)	
			.Add(fScrollView))
		//.AddGlue()
		//.SetInsets(10, 0, 0, 0)
	);
	BSize size;
	size = GetLayout()->View()->PreferredSize();
	SetSizeLimits(size.Width(), 1000, 100, 248);
	
	// NODE MANAGER why here?
	fNodeManager = new NodeManager();
	fNodeManager->SetTarget(this);

	// 	OTHER WINDOWS	
	// CONTROLS
	fControlsWindow = new ControlsWindow();
	//fControlsWindow->SetTarget(this);
	fControlsWindow->Show();
	
	// AUDIO MIXER
	fAudioMixerWindow = new BWindow(BRect(750, 350, 1250, 450), "Audio mixer", B_FLOATING_WINDOW_LOOK, B_FLOATING_APP_WINDOW_FEEL,
		B_NOT_ZOOMABLE | B_ASYNCHRONOUS_CONTROLS);
	fAudioMixerWindow->Show();	// This is weird, but we need to show and hide, otherwise it crashes in MessageReceived when trying to show
	fAudioMixerWindow->Hide();	// Could be because window's message loop is not started until Show() is called
	
	// MIDI MIXER
	fMidiMixerWindow = new BWindow(BRect(750, 500, 1250, 600), "MIDI mixer", B_FLOATING_WINDOW_LOOK, B_FLOATING_APP_WINDOW_FEEL,
		B_NOT_ZOOMABLE | B_ASYNCHRONOUS_CONTROLS);	
	//fMidiMixerWindow->SetLayout(new BGroupLayout(B_HORIZONTAL));
	//MidiMixerView* midiMixerView = new MidiMixerView();
	//fMidiMixerWindow->GetLayout()->AddView(midiMixerView); // a long way of saying fMidiMixerWindow->AddChild(midiMixerView)

	//fMidiMixerWindow->Show();
	//fMidiMixerWindow->Hide();
}

void			//MESSAGE_RECEIVED
MainWindow::	MessageReceived(BMessage * msg)
{
	switch (msg->what)
	{
		case CONNECT_MSG:
		{
			//TODO ADD METHODS TO NODEMANAGER HERE
			/*
			// Parameter web for ToneProducer and show a control view for it
			BParameterWeb* web;
			fRoster->GetParameterWebFor(mConnection.producer, &web);
			BView* view = BMediaTheme::ViewFor(web);
			BWindow* controlsWin = new BWindow(BRect(250, 200, 300, 300), "Controls",
				B_TITLED_WINDOW, B_ASYNCHRONOUS_CONTROLS);
			controlsWin->AddChild(view);
			controlsWin->ResizeTo(view->Bounds().Width(), view->Bounds().Height());
			controlsWin->Show();*/
		}
		
		case MSG_PLAY_PAUSE:	// TODO: MAKE THE CLOCK ACTUALLY STOP!!!
		{
			if (fPlaybackState == PLAYBACK_STATE_STOPPED) {
				if (fClockRunning == false) {
					fMediaClock = new MediaClock(this, 10);
					fClockRunning = true;
				}
				fPlaybackState = PLAYBACK_STATE_PLAYING;
			} else if (fPlaybackState == PLAYBACK_STATE_PLAYING)
				fPlaybackState = PLAYBACK_STATE_PAUSED;
			else
				fPlaybackState = PLAYBACK_STATE_PLAYING;
			break;
		}
		
		case MSG_STOP:			// TODO: MAKE THE CLOCK ACTUALLY STOP!!!
		{
			fPlaybackState = PLAYBACK_STATE_STOPPED;
			fTickPosition = 0;
			fTick = 0;
			
			int *val2;
			if ( fPlayingNotesList.CountItems() > 0 ) {
				for (int j = 0; j < fPlayingNotesList.CountItems()
				&& fPlayingNotesList.CountItems() > 0; j++) {
					val2 = (int *)fPlayingNotesList.ItemAt(j);
					//printf("DEBUG: OFFNOTE at: %d %d %d \n", (int)*val2, (int)*(val2+1), (int)*(val2+2) );
					midi_out->SprayNoteOff( (uchar) 0, (uchar) (*(val2+1)-1), (uchar) 100 );
				} //for
			} //if
			fPlayingNotesList.MakeEmpty();
			break;
		}
		
		case MSG_SKIP_FORWARD:
		{
			if ( (fPlaybackState == PLAYBACK_STATE_STOPPED
			|| fPlaybackState == PLAYBACK_STATE_PAUSED)
			&& fTickPosition < 64 ) {	
				printf("fTickPosition: %d, fRealResolution: %d \n", fTickPosition, fRealResolution);
				if (fTickPosition%(64/fRealResolution) == 0)					//bad?
					fTickPosition = fTickPosition + 64/fRealResolution;
				else
					fTickPosition = fTickPosition + (64/fRealResolution-fTickPosition%(64/fRealResolution));	//good
				fTickView->SetPosition(fTickPosition);
				fTickView->Invalidate();
			} //if
			break;
		}

		case MSG_SKIP_BACKWARDS:
		{
			
			if ( (fPlaybackState == PLAYBACK_STATE_STOPPED || fPlaybackState == PLAYBACK_STATE_PAUSED)
			&& fTickPosition > 0 ) {	
				printf("MainWindow: fTickPosition: %d, fRealResolution: %d \n", fTickPosition, fRealResolution);
				
				if (fTickPosition%(64/fRealResolution) == 0)
					fTickPosition = fTickPosition - 64/fRealResolution;
				else
					fTickPosition = fTickPosition - fTickPosition%(64/fRealResolution);
				fTickView->SetPosition(fTickPosition);
				fTickView->Invalidate();
			}	//if
			break;
		}
		
		case MSG_RECORD:
		{
			printf("MainWindow: MSG_RECORD \n");
			int32 note;
			if (msg->FindInt32("note", &note) == B_OK) {
				printf("MainWindow: Record note: %d \n", (int) note);
				printf("MainWindow: trying to post record to nodemanager \n");
				fNodeManager->_Record(msg);
			}
			break;
		}
		
		case RESOLUTION_MSG:
		{
			printf("MainWindow: Resize received \n");
			int32 size;
			if (msg->FindInt32("Resolution", &size) == B_OK) {
				fRealResolution = (int) pow(2, size);
			}
			PostMessage(msg, fPatternView);
			//directly calling _Record works. Why?
			break;
		}
		
		case MSG_TEMPO:
		{
			int32 tempo;
			if (msg->FindInt32("Tempo", &tempo) == B_OK) {
				double tempo2;
				tempo2 = (double) tempo*4;
				tempo2 = tempo2/15;
				
				printf("MainWindow: MSG_TEMPO %f \n", (const double) tempo2);
				fMediaClock->SetInterval((const double) tempo2 );
				//remove later
			}	
			break;
		}
		
		case SAMPLE_MESSAGE:	//TODO: Tidy up a bit
		{
			int32 value;
			if (msg->FindInt32("note", &value) == B_OK) {
				printf("BUTTON MESSAGE: %lu \n", value);				
				Play();		//TODO: Add things to PLAY
			}
			else printf("Nie udalo sie \n");
			break;
		}
		case B_SIMPLE_DATA:
		{
			printf("MainWindow: Files received! \n");
			entry_ref ref;
			int ref_num = 0;
			do {
				if ( msg->FindRef("refs", ref_num, &ref) != B_OK ) {
					printf("MainWindow: Error finding the refs! That is, the sample files. \n");
					break;
				}
				ref_num++;
			} while (1);
			
			//X fEditView->SetSamplesNumber(ref_num);
			//X fEditView->Invalidate();
			//X fSampleView->SetSamplesNumber(ref_num);
			//X fSampleView->Invalidate();
			
			//fSamplesAdded(ref_num);
			//the next is tricky, the above somehow doesn't work
			//fSamplesNumber = ref_num;
			//fSamplesAdded(fSamplesNumber);
			break;
		}
		case MSG_MIDI_NOTEON:
		{			
			ssize_t dataSize;
			uchar *note;
			msg->FindData("note", B_INT32_TYPE, (const void**)&note, &dataSize);
			
			printf("MainWindow: Note_on received. Note: %d \n",(int) *note);
			PostMessage(msg, fNodeManager);
			printf("MainWindow: Message posted \n");
			break;
		}
		
		case NOTEOFF_MSG:	//Same as above
		{	
			ssize_t dataSize;
			uchar *note;
			msg->FindData("note", B_INT32_TYPE, (const void**)&note, &dataSize);
			printf("MainWindow: Note_off received. Note: %d \n",(int) *note);
			PostMessage(msg, fNodeManager);
			printf("MainWindow: Message posted \n");
			break;
		}
		// MENU MESSAGES
		case MSG_MENU_QUIT:
		{
			printf("Quitting! \n");
			//if (midi_out)	midi_out->Release();
			//else			printf("Midi out was not taken, so not releasing. \n");
			//if (midi_in)	midi_in->Release();
			//else			printf("Midi in was not taken, so not releasing. \n");
			
			be_app->PostMessage(B_QUIT_REQUESTED);
			break;
		}
		case MENU_AUDIO_MIXER:	// FIX: The if/else is very awkward here. ADD: Quit_requested->setMarked(false)
		{
			printf("MainWindow: Menu: Audio mixer pressed \n");
			BMenuItem* item = fMenuBar->FindItem("Audio mixer");
			if ( item->IsMarked() == false ) {
				item->SetMarked(true);
				fAudioMixerWindow->Show();	
			}
			else if ( item->IsMarked() == true) {
				printf("hello \n");
				item->SetMarked(false);
				// if is hidden=false then hide
				fAudioMixerWindow->Hide();	
			}
			else {	// Window was closed manually
				printf("Something's wrong \n");
			}
				
			break; 
		}
		case MENU_MIDI_MIXER:	// FIX: The if/else is very awkward here. ADD: Quit_requested->setMarked(false)
		{
			printf("MainWindow: Menu: MIDI mixer pressed \n");
			BMenuItem* item = fMenuBar->FindItem("MIDI mixer");
			if ( item->IsMarked() == false && fMidiMixerWindow->IsHidden() == true) {
				item->SetMarked(true);
				fMidiMixerWindow->Show();	
			}
			else if ( item->IsMarked() == true && fMidiMixerWindow->IsHidden() == false) {
				item->SetMarked(false);
				fMidiMixerWindow->Hide();	
			}
			else {	// Window was closed manually
				fMidiMixerWindow->Show();
			}	
			break; 
		}
		case MENU_LIBRARY:
		{
			printf("MainWindow: Menu: Library pressed \n");
			BMenuItem* item = fMenuBar->FindItem("Library");
			( item->IsMarked() == true )? item->SetMarked(false) : item->SetMarked(true);
			break; 
		}
		
		case MSG_ADD_NODE:
		{
			printf("MainWindow: New node \n");
			int32 newX, newY;
			if (msg->FindInt32("x", &newX) == B_OK && msg->FindInt32("y", &newY) == B_OK ) {
				SampleTrack* track = (SampleTrack*) fSamplesList.ItemAt((int)newY);
				track->AddRemoveNode((int)newX);
				fPatternView->fEditView->Invalidate();
			}
			break;	
		}
		
		case MSG_FILL:
		{
			printf("MainWindow: MSG_FILL \n");
			int32 note, freq;
			if ( (msg->FindInt32("note", &note) == B_OK) && (msg->FindInt32("fillNumber", &freq) == B_OK) ) {
				//printf("DEBUG: NOTE: %d \n", (int) note);
				SampleTrack* track = (SampleTrack*) fSamplesList.ItemAt((int)note);
				track->Fill((int)freq);
				fPatternView->fEditView->Invalidate();
			}
			break;		
		}
		
		case MSG_CLEAR:
		{
			printf("MainWindow: MSG_CLEAR \n");
			int32 note;
			if ( msg->FindInt32("note", &note) == B_OK ) {
				//printf("DEBUG: NOTE: %d \n", (int) note);
				SampleTrack* track = (SampleTrack*) fSamplesList.ItemAt((int)note);
				track->Clear();
				fPatternView->fEditView->Invalidate();
			}
			break;
		}
		
		default:
		{
			printf("MainWindow: Unknown MSG \n");
			break;
		}
	} //endcase
}//MessageReceived

void
MainWindow::		Play()
{
	// LEAVEBMediaRoster* fRoster = BMediaRoster::Roster();
	printf("Playing! \n");
	//The following crashes on exit because something is not released or the fPlayer not stopped
	// LEAVE status_t error;
	// LEAVE error = fRoster->GetAudioMixer(&fAudioMixerNode);	//assign audio mixer to fAudioMixer, return error
	// LEAVE if (error < B_OK)
	// LEAVE	printf("There was an error when finding the mixer node! \n");
		
	//BEntry& entry = item->Entry();
	//entry_ref ref;
	//entry.GetRef(&ref);
	//fPlayFile = new BMediaFile(&ref); //, B_MEDIA_FILE_UNBUFFERED);
	//if ((error = fPlayFile->InitCheck()) < B_OK) {
	//	delete fPlayFile;
		//fPlayFile = NULL;
		//return error;
	//}
	//create NODE
	// LEAVE fPlayer = new BSoundPlayer(fAudioMixerNode, &fPlayFormat.u.raw_audio, "Sound Player");
	// LEAVE fPlayer->Start();
}

void 
MainWindow::Pulse()	//TODO: CHANGE FOR THE NEW LIST
{
	//printf("MainWindow:: Pulse() \n");
	if (fPlaybackState == PLAYBACK_STATE_STOPPED)
		{
		} else if (fPlaybackState == PLAYBACK_STATE_PLAYING) {
			int xPosition = fTickPosition;
			SampleTrack* track;
			for (int i = 0; i < fSamplesList.CountItems(); i++) {
				//val = (int *) fPatternView->fEditView->fNodeList.ItemAt(j);
				track = (SampleTrack*) fSamplesList.ItemAt(i);
		
				if (track->ItemAt(fTickPosition) == true) {
					printf("NOTE! %d : %d \n", fTickPosition, i);
					uchar channel, note, velocity;
					channel = (uchar) 0;
					note = (uchar) i;
					velocity = 100;
					midi_out->SprayNoteOn(channel, note, velocity);
					
					// DURATION
					int duration;
					duration = 10;	// TODO: Add method
					
					struct position	//is it needed? use sampleCoords!
					{
						int x;
						int y;
						int z;
					};
					position *pos = new position[3];
					pos[0].x = xPosition;
					pos[0].y = i;
					pos[0].z = duration + fTick;
					fPlayingNotesList.AddItem(pos);
				}	//if
			}	//for
			
			// SEND OFFNOTES
			// Count how many note offs we need to send (for now, off them at the same time)
			int count = 0;
			int *val2;
			if ( fPlayingNotesList.CountItems() > 0 ) {
				for (int j = 0; j < fPlayingNotesList.CountItems() && fPlayingNotesList.CountItems() > 0; j++) {
					val2 = (int *)fPlayingNotesList.ItemAt(j);
					if ( (int)*(val2+2) == fTick ) {
						midi_out->SprayNoteOff( (uchar) 0, (uchar) (*(val2+1)), (uchar) 100 );
						printf("Spraying off: %d \n", *(val2+1) );
						count++;
					} //if
				} //for
			} //if
			
			// REMOVE FROM LIST TODO: Add logic: if a note is already playing but needs a note-on, send a quick note-off before sending another note-on
			for ( int k = 0; k < count; k++ ) {
				if ( fPlayingNotesList.CountItems() > 0 ) {
					for (int j = 0; j < fPlayingNotesList.CountItems() && fPlayingNotesList.CountItems() > 0; j++) {
						val2 = (int *)fPlayingNotesList.ItemAt(j);
						if ( (int)*(val2+2) == fTick ) {
							fPlayingNotesList.RemoveItem(j);
							break;
						} //if
					} //for
				} //if
			} //for
			
			fTick++;
			if		(fTickPosition < 63) fTickPosition++;
			else	fTickPosition = 0;
		}
	fTickView->SetPosition(fTickPosition);	
	PostMessage(new BMessage(MSG_TICK), fTickView);
}

void
MainWindow::_MakeMenuBar()
{
	BMenuItem* menuItem;
	fMenuBar = new BMenuBar(BRect(0, 0, 0, 0), "menubar");
	
	// FILE
	BMenu* fileMenu = new BMenu("File");
	fMenuBar->AddItem(fileMenu);
		// NEW
		BMenu* newMenu = new BMenu("New");
			BMessage* fileMessage = new BMessage('bla');
			newMenu->AddItem(menuItem = new BMenuItem("Empty song", fileMessage));
			menuItem = new BMenuItem(newMenu, new BMessage('mnew'));
			menuItem->SetShortcut('N', B_COMMAND_KEY);
			fileMenu->AddItem(menuItem);
		// SAVE
		fileMenu->AddItem(menuItem = new BMenuItem("Save", new BMessage('savE')), 1);
		menuItem->SetShortcut('S', B_COMMAND_KEY);
		// SAVE AS
		fileMenu->AddItem(menuItem = new BMenuItem("Save as...", new BMessage('savN')), 2);
		// QUIT
		fileMenu->AddSeparatorItem();
		fileMenu->AddItem(menuItem = new BMenuItem("Quit", new BMessage(MSG_MENU_QUIT), 'Q'));

	// MEDIA MODE 
	fAudioMenu = new BMenu("Mode");
	fAudioMenu->SetRadioMode(true);
	fMenuBar->AddItem(fAudioMenu);
		fAudioMenu->AddItem(menuItem = new BMenuItem("Tone Producer (Synth)", new BMessage(CONNECT_MSG)), 0);
		fAudioMenu->AddItem(menuItem = new BMenuItem("Sampler", new BMessage(CONNECT_MSG)), 1);
		menuItem->SetMarked(true);
	
	// WINDOWS
	fWindowsMenu = new BMenu("Windows");
	fMenuBar->AddItem(fWindowsMenu);
		fWindowsMenu->AddItem(menuItem = new BMenuItem("Library", new BMessage(MENU_LIBRARY)), 0);
		fWindowsMenu->AddItem(menuItem = new BMenuItem("Audio mixer", new BMessage(MENU_AUDIO_MIXER)), 1);
		fWindowsMenu->AddItem(menuItem = new BMenuItem("MIDI mixer", new BMessage(MENU_MIDI_MIXER)), 2);
	
	// SETTINGS
	fSettingsMenu = new BMenu("Settings");
	fMenuBar->AddItem(fSettingsMenu);
		// LOOK
		BMenu* lookMenu = new BMenu("Look");
			lookMenu->AddItem(menuItem = new BMenuItem("look1", new BMessage('lok1')), 0);
		menuItem = new BMenuItem(lookMenu, new BMessage('mnew'));
		fSettingsMenu->AddItem(menuItem);
			lookMenu->AddItem(menuItem = new BMenuItem("look2", new BMessage('lok1')), 1);
		fSettingsMenu->AddItem(menuItem = new BMenuItem("Inputs", new BMessage('inpu')), 1);
}
