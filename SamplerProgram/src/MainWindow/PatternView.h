#ifndef PATTERNVIEW_H
#define PATTERNVIEW_H

#include <View.h>

#include "MainWindow/EditView.h"
//#include "MainWindow/InstrumentView.h"
#include "MainWindow/GView.h"
#include "MainWindow/TickView.h"
#include "List.h"

class PatternView:	public BGroupView {
public:
					PatternView(BList* samplesList);
	virtual void	Draw(BRect updateRect);
	void			MessageReceived(BMessage* message);
	virtual	void	SetSamplesNumber(float number);
	BList			fNodeList;
	EditView*		fEditView;
	
	void 			SetTarget(BHandler* handler);

private:
	BMessenger*		fMessenger;
	BMessageRunner*	fMessageRunner;
	TickView*		fTickView;
	//InstrumentView*	fInstrumentView;
	GView*			fGView;
	
	int				fRealResolution;
	int				fSampleHeight;
	int				fSampleWidth;
	int				fSamplesNumber;	
	int				fSampleDuration;
	int				fWindowHorizShift;
};

#endif // PATTERNVIEW_H
