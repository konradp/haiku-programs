#include <stdio.h>
#include <Button.h>
#include <Point.h>

#include <GroupLayout.h>

#include <StorageKit.h>
#include <String.h>

#include "MainWindow.h"
#include "InstrumentView.h"
#include "buttons/SampleButton.h"
#include "support/messages.h"

#include <media/TimeSource.h>
#include <media/MediaTheme.h>

#include <Box.h>
#include <GroupLayoutBuilder.h>
#include <LayoutBuilder.h>


InstrumentView::InstrumentView()
	:	BView(BRect(0,0,1,1), "InstrumentView", B_FOLLOW_LEFT | B_FOLLOW_TOP, B_WILL_DRAW ),
		fSamplesNumber(8)
		//fButtonLayout(new BGroupLayout(B_HORIZONTAL, 0))
{
	//SetLayout(fButtonLayout);
	
	//fExpandGroupExpandedShape = fShapes->ExpandGroupExpandedShape(15);
	//fExpandGroupCollapsedShape = fShapes->ExpandGroupCollapsedShape(15);
}

InstrumentView::~InstrumentView()
{
}

//AttachedToWindow	TODO: REMOVE THIS in favour of SetSamplesNumber.
void
InstrumentView::AttachedToWindow()
{	
	//fSampleStatusList.MakeEmpty();

	//BGroupLayoutBuilder* layoutBuilder = new BGroupLayoutBuilder(B_VERTICAL, 0);
	/*

	BMessage* buttonMessage;
	// ExpandGroupButton
	buttonMessage = new BMessage(MSG_EXPAND_GROUP);
	buttonMessage->AddInt32("group", 0);
	fDownArrowButton = new TwoSymbolButton(0, B_EMPTY_STRING, fExpandGroupExpandedShape, fExpandGroupCollapsedShape, buttonMessage);
	

	//fButtonLayout->AddView(fDownArrowButton);
	//layoutBuilder->Add(fDownArrowButton);
	// Draw the buttons for each of the samples
	for ( int i = 0; i < fSamplesNumber; i++ )
	{
		int *NoteStatus;
		NoteStatus = new int(0);
		fSampleStatusList.AddItem(NoteStatus);
		
		//Sample button
		char name[256];
		sprintf(name, "Sample %d", i);
		
		//fSampleButtonView = new SampleButtonView(i);
		//fButtonLayout->AddView(fSampleButtonView);
		//layoutBuilder->Add(fSampleButtonView);
	}*/
}

//DRAW
/* This is refreshed with Invalidate(), eg. automatically when the window is moved or resized */
void
InstrumentView::		Draw(BRect updateRect)
{
}

//Message Received
void
InstrumentView::		MessageReceived(BMessage * msg)
{
	switch (msg->what)
	{		
	/*	case SAMPLE_MESSAGE:	//TODO: Tidy up a bit. This message should be posted to the main window
		{
			int32 value;			
			if (msg->FindInt32("note", &value) == B_OK) {
				printf("BUTTON MESSAGE: %lu \n", value);
				int i;
				i = (int) value;
				
				//change in the list
				int *pointer;
				pointer = (int*) fSampleStatusList.ItemAt(i-1);
				if ((int) *pointer == 0) *pointer = 1;
				else *pointer = 0;
				
				Invalidate();
			} else
				printf("Nie udalo sie \n");
			break;
		}
		case MSG_MIDI_NOTEON:
		{			
			ssize_t dataSize;
			uchar *note;
			msg->FindData("note", B_INT32_TYPE, (const void**)&note, &dataSize);
			printf("InstrumentView: Note_on received. Note: %d \n",(int) *note);
			
			if (*note < fSamplesNumber) {
				int *pointer;
				pointer = (int*) fSampleStatusList.ItemAt((int) *note);	//accessing the status of the corresponding sample in the list
				*pointer = 1;	//1 means 'being played', TODO: change to bool type (true/false)
				Invalidate();	//refresh window. TODO: refresh only the indicator box next to the sample, not the whole window
			}
			break;
		}
		case NOTEOFF_MSG:	//Same as above
			ssize_t dataSize;
			uchar *note;
			msg->FindData("note", B_INT32_TYPE, (const void**)&note, &dataSize);
			printf("InstrumentView: Note_off received. Note: %d \n",(int) *note);
			
			if (*note < fSamplesNumber) {
				int *pointer;
				pointer = (int*) fSampleStatusList.ItemAt((int) *note);
				*pointer = 0;
				Invalidate();
			}
			break;
			
		case MSG_EXPAND_GROUP:
		{
			printf("InstrumentView: Expand group \n");
			
			int32 value;			
			if (msg->FindInt32("note", &value) == B_OK) {
				printf("BUTTON MESSAGE: %lu \n", value);
				int i;
				i = (int) value;
				
				//change in the list
				//pointer = (int*) fSampleStatusList.ItemAt(i-1);
				//if ((int) *pointer == 0) *pointer = 1;
				//else *pointer = 0;
				
				//fSampleButtonView->GroupButtonAt(i)->fEnabled = false;
				
				Invalidate();
			} else
				printf("Nie udalo sie \n");
			
			
			
			break;
		}
				
		case MSG_FILL:
		{
			printf("InstrumentView: Fill \n");
			BView::MessageReceived(msg);
			break;
		}
		
		case MSG_CLEAR:
		{
			printf("InstrumentView: Clear \n");		
			//BView::MessageReceived(msg);
		}
			
		default:
		{
			BView::MessageReceived(msg);
			printf("InstrumentView: UNKNOWN MESSAGE \n");
			break;
		}*/
	}
}

//fSamplesAdded
void
InstrumentView::SetSamplesNumber(int number)
{
/*	fSamplesNumber = number;
	//clear the list
	for (int j = 0; j < fButtonList.CountItems(); j++)
	{
		RemoveChild((BButton*)fButtonList.ItemAt(j));
	}
	fButtonList.MakeEmpty();
	
	BRect rect(0, 20, 90, 44);
	BButton* button;
	for ( int i = 1; i <= fSamplesNumber; i++ )
	{
		char name[256];
		sprintf(name, "Sample %d", i);
		
		BMessage* buttonMsg;
		buttonMsg = new BMessage(SAMPLE_MESSAGE);
		
		float value;
		value = (float) i;
		buttonMsg->AddFloat("_size", value);
		
		button = new BButton(rect, name, name, buttonMsg);
		button->SetTarget(this);
		fButtonList.AddItem(button);
		AddChild(button);
		//AddChild((BButton*)fButtonList.ItemAt(i-1));
		rect.OffsetBy(0, 24);
	}*/
}
