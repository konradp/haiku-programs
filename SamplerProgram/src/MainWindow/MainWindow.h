#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "MainWindow/EditView.h"
#include "MidiMixerView.h"
#include "MainWindow/PatternView.h"
//#include "MainWindow/SampleView.h"
#include "MainWindow/TickView.h"
#include "MainWindow/TransportView.h"
#include "media_nodes/NodeManager.h"

#include "ControlsWindow.h"
#include "SampleTrack.h"

#include <View.h>
#include <ObjectList.h>
#include <MidiConsumer.h>
#include <MediaFile.h>

#include <MidiProducer.h>
#include <MediaRoster.h>
#include <SoundPlayer.h>

class BMenu;
class MediaClock;

class MainWindow:	public BWindow {
public:
					MainWindow(BRect frame);
					~MainWindow();
	void			MessageReceived(BMessage* message);
	
	void			Play();
	virtual	void	Pulse();
	MediaClock*		fMediaClock;

private:
	void				CreateViews();
	BScrollView*		fScrollView;
	BView*				fMainView;
	TickView*			fTickView;
	PatternView*		fPatternView;
	TransportView*		fTransportView;
	ControlsWindow*		fControlsWindow;
	BWindow*			fAudioMixerWindow;
	BWindow*			fMidiMixerWindow;
	
private:

	// MENU BAR
	void				_MakeMenuBar();
	BMenuBar*			fMenuBar;
	BMenu*				fAudioMenu;
	BMenu*				fWindowsMenu;
	BMenu*				fSettingsMenu;
	
	int					fPlaybackState;
	bool				fClockRunning;
	int32				fResolution;
	int					fRealResolution;
	int					fSamplesNumber;
	int					fTickPosition;
	int					fTick;
	
	BList				fButtonList;
	BList				fEntryRefsList;
	BList				fPlayingNotesList;
	
	// MIDI
	BMidiLocalConsumer*	midi_in;
	BMidiLocalProducer*	midi_out;

private:
	BSoundPlayer*		fPlayer;
	BMediaFile*			fPlayFile;
	media_format		fPlayFormat;
	
	NodeManager*		fNodeManager;
private:
	BList				fSamplesList;
};

#endif // MAINWINDOW_H
