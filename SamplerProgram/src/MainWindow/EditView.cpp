/* EditView:	This is the area where we draw out dots (notes). We can draw max. 64 samples in a line (64 per sample).
				For now this number is hard-coded. The note positions are numbered 0-65 and stored in a list.
				In order to be able to draw a full dot (and not a half of it), the coordinates are shifted by number 8,
				which is also hard-coded for now	*/

#include <stdio.h>

#include "support/messages.h"
#include "EditView.h"

#include <StorageKit.h>
#include <MessageRunner.h>
#include <Messenger.h>

EditView::	EditView(BList* samplesList)
	//:	BView(BRect(0,0,1,1), "Pattern Edit Frame", B_FOLLOW_LEFT | B_FOLLOW_TOP, B_WILL_DRAW),
	:	BView("Pattern Edit Frame", B_WILL_DRAW),
		//fNode(10,10),			// pos. of node
		fResolution(3),			// no. of lines
		fRealResolution(8),		// real no. of lines 2^3=8
		fSampleHeight(24),		// sample height 24 originally
		fSamplesNumber(8),		// no. of samples
		fSampleDuration(4),		// default note duration
		fSamplesList(NULL),
		fWindowHorizShift(8)	//so that the whole node is drawn
{
	fSamplesList = samplesList;
}

//Draw
void
EditView::		Draw(BRect updateRect)
{
	// FillRect(updateRect, B_SOLID_LOW);
	pattern stripes = { {0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00} };
	
	fSampleHeight = int(be_plain_font->Size()*2.33) | 1;
	fSampleWidth = (64*8)/fRealResolution;	// TODO: Move calculations out of DRAW()
	
	// BORDER
	// BRect aRect((Bounds().LeftTop().x + 8), 0, Bounds().RightTop().x, fSampleHeight*fSamplesNumber);
	BRect aRect((Bounds().LeftTop().x + fWindowHorizShift), 0, 64*8+8, fSampleHeight*fSamplesNumber);
	StrokeRect(aRect);


	for (int i = 0; i < fSamplesNumber; i++)
	{	//HORIZONTAL LINES
		BPoint shiftPoint0(0, i*fSampleHeight);
		StrokeLine(aRect.LeftTop() + shiftPoint0,
		aRect.RightTop() + shiftPoint0);
	}
	for (int i = 1; i < fRealResolution; i++)
	{	//VERTICAL LINES
		BPoint shiftPoint1((i*fSampleWidth),0);
		StrokeLine(aRect.LeftTop() + shiftPoint1,
		aRect.LeftBottom() + shiftPoint1, stripes);
	}
	
	// DRAWING ALL POINTS
	SampleTrack* track;
	BPoint Node;
	//for (int i = 0; i < 64 && fTimeList[i].IsEmpty() == false; i++)	{ //loop through each point in the list
	for ( int j = 0; j < fSamplesList->CountItems(); j++ ) {
		for (int i = 0; i < 64; i++)	{ //loop through each point in the list
			track = (SampleTrack *)fSamplesList->ItemAt(j);
			if ( track->ItemAt(i) != false ) {
				//printf("DEBUG: EditView: ITEM_LIST %d: %d %d \n", i, *val, *(val+1));
				Node.x = i*8 + fWindowHorizShift;	//TODO: 8 SampleWidth hard-coded
				Node.y = j*fSampleHeight + fSampleHeight/2;
				FillEllipse(Node, 5, 5);	
			} // if
		} // for
	} // for
}

// Mouse Down
void
EditView::		MouseDown(BPoint where)
{
	printf("EditView: MouseDown() \n");
	
	if (where.y <= fSampleHeight*fSamplesNumber)
	{
		int newX = ((int)where.x - fWindowHorizShift )/(int)fSampleWidth;
		int newY = (int) (where.y/fSampleHeight);
		
		printf("X: %f \n", where.x);
		printf("X: %f \n", where.x/8);
		printf("shift: %d \n", fWindowHorizShift);
		printf("fRealResolution: %d \n", fRealResolution);
		printf("fSampleWidth: %d \n", fSampleWidth);
		printf("NEWX: %d \n", newX);
		printf("------: %d \n", newX*(64/fRealResolution));
		newX = newX*(64/fRealResolution);

		BMessage* msg = new BMessage(MSG_ADD_NODE);
		msg->AddInt32("x", (int32) newX);
		msg->AddInt32("y", (int32) newY);
		BView::MessageReceived(msg);	// TODO: Is this the right way of sending a message?
	}
}

//SetResolution
void
EditView::SetResolution(int size)
{
	fResolution = size;
	fRealResolution = (int) pow(2, fResolution);
	Invalidate();
}

//SetSamplesNumber
void
EditView::SetSamplesNumber(float number)
{fSamplesNumber = (int) number;}


