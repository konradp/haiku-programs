#ifndef GROUPVIEW_H
#define GROUPVIEW_H

#include <GroupView.h>
#include <GridView.h>
#include "buttons/SampleButton.h"

class BGroupLayout;

class GView:		public BGroupView {
public:
						GView();
						~GView();
		virtual void	Draw(BRect updateRect);
		
		BSize			MinSize();
		void			DisplayMenu(BPoint where, int32 id, SampleButton* button = NULL) const;

								
private:
	int					fSamplesNumber;
	//BGroupLayout*		fButtonLayout;

};


#endif // _H
