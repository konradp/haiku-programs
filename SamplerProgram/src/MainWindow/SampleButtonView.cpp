/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#include "SampleButtonView.h"
#include "support/messages.h"

#include <stdio.h>
#include <GroupLayout.h>
#include <MenuItem.h>
#include <PopUpMenu.h>

SampleButtonView::SampleButtonView(int32 id)
	:	BView(BRect(0,0,1,1), "SampleButtonView", B_FOLLOW_LEFT | B_FOLLOW_TOP, B_WILL_DRAW ),
		fButtonLayout(new BGroupLayout(B_HORIZONTAL, 0)),
		fSampleHeight(24)	// sample height
{
	fSymbolHeight = int(be_plain_font->Size() / 2) | 1;
	fRecordShape = fShapes->RecordShape(15);

	
	SetLayout(fButtonLayout);
	fButtonLayout->SetInsets(0, 0, 0, 0);
	
	// SampleButton
	char name[256];
	sprintf(name, "Sample %lu", id);	//i instead of 2
	
	BMessage* buttonMessage;

	// SampleButton
	buttonMessage = new BMessage(SAMPLE_MESSAGE);
	buttonMessage->AddInt32("note", id);
	fSampleButton = new SampleButton(id, name, name, buttonMessage);
	
	// RecordButton
	buttonMessage = new BMessage(MSG_RECORD);
	buttonMessage->AddInt32("note", id);
	//fRecordButton = new RecordButton(id, B_EMPTY_STRING, fRecordShape, buttonMessage);
	//fRecordButton = new TwoSymbolButton(id, B_EMPTY_STRING, fRecordShape, fRecordShape, buttonMessage);
		
	fButtonLayout->AddView(fSampleButton);
	fButtonLayout->AddView(fRecordButton);
}

SampleButtonView::	~SampleButtonView()
{	
}

void
SampleButtonView::DisplayMenu(BPoint where, int32 id, SampleButton* button) const
{
	printf("Sample clicked: %lu \n", (int32) id);
	
	BPopUpMenu* sampleMenu = new BPopUpMenu("SampleMenu", false, false);
	BMessage* message = new BMessage(MSG_FILL);
	BMenuItem* item = new BMenuItem("Empty item", message);
	
	//FILL MENU
	BMenu* fillMenu = new BMenu("Ffill...");
		int32 note = (int32) id;
		
		// 2
		message = new BMessage(MSG_FILL);
		message->AddInt32("note", note);
		message->AddInt32("fillNumber", 2);
		item = new BMenuItem(("2"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 4
		message = new BMessage(MSG_FILL);
		message->AddInt32("note", note);
		message->AddInt32("fillNumber", 4);
		item = new BMenuItem(("4"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 8
		message = new BMessage(MSG_FILL);
		message->AddInt32("note", note);
		message->AddInt32("fillNumber", 8);
		item = new BMenuItem(("8"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 16
		message = new BMessage(MSG_FILL);
		message->AddInt32("note", note);
		message->AddInt32("fillNumber", 16);
		item = new BMenuItem(("16"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 32
		message = new BMessage(MSG_FILL);
		message->AddInt32("note", note);
		message->AddInt32("fillNumber", 32);
		item = new BMenuItem(("32"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 64
		message = new BMessage(MSG_FILL);
		message->AddInt32("note", note);
		message->AddInt32("fillNumber", 64);
		item = new BMenuItem(("64"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
	sampleMenu->AddItem(fillMenu);
	// END FILL MENu
	
	
	// CLEAR BUTTON
	message = new BMessage(MSG_CLEAR);
	message->AddInt32("note", note);
	item = new BMenuItem(("Clear"), message);
	item->SetTarget(this);
	sampleMenu->AddItem(item);
	
	// ADD ONE BELOW BUTTON
	message = new BMessage('ad1b');
	item = new BMenuItem(("Add one below"), message);
	item->SetTarget(this);
	sampleMenu->AddItem(item);
	
	// MENU methods
	where = ConvertToScreen(where);
	BRect mouseRect(where, where);
	mouseRect.InsetBy(-4.0, -4.0);
	sampleMenu->Go(where, true, false, mouseRect, true);
	
}
