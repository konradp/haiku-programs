#include "GView.h"
#include <GroupLayout.h>
#include <GridLayout.h>
#include <GroupLayoutBuilder.h>
#include <Layout.h>
#include <Button.h>
#include <stdio.h>
#include <PopUpMenu.h>
#include "support/messages.h"
#include <MenuItem.h>




GView::GView()
	:	BGroupView(B_VERTICAL, 0),
		fSamplesNumber(4)
		//fButtonLayout(new BGroupLayout(B_VERTICAL, 0))
{
	//SetLayout(fButtonLayout);
	BGridView* buttonGroup = new BGridView(B_VERTICAL, 0);
	BGridLayout* buttonLayout = buttonGroup->GridLayout();
	//GroupLayout
	GroupLayout()->AddView(buttonGroup, 0);
	
	BButton* button;
	for ( int i = 0; i < 4; i++ ) {
		button = new BButton("V");
		buttonLayout->AddView(button, 0, i*fSamplesNumber);
		
		for ( int j = 0; j < fSamplesNumber; j++ ) {
			button = new BButton("Sample");
			buttonLayout->AddView(button, 1, i*fSamplesNumber+j);

		}		
		
	}
}

void
GView::Draw(BRect updateRect)
{
	StrokeRect(Bounds());
}

GView::~GView()
{
}

BSize
GView::MinSize()
{
	BSize size(100, 100);
	return size;
}

void
GView::DisplayMenu(BPoint where, int32 id, SampleButton* button) const
{
	printf("Sample clicked: %lu \n", (int32) id);
	
	BPopUpMenu* sampleMenu = new BPopUpMenu("SampleMenu", false, false);
	BMessage* message = new BMessage(MSG_FILL);
	BMenuItem* item = new BMenuItem("Empty item", message);
	
	//FILL MENU
	BMenu* fillMenu = new BMenu("Ffill...");
		int32 note = (int32) id;
		
		// 2
		message = new BMessage(MSG_FILL);
		message->AddInt32("note", note);
		message->AddInt32("fillNumber", 2);
		item = new BMenuItem(("2"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 4
		message = new BMessage(MSG_FILL);
		message->AddInt32("note", note);
		message->AddInt32("fillNumber", 4);
		item = new BMenuItem(("4"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 8
		message = new BMessage(MSG_FILL);
		message->AddInt32("note", note);
		message->AddInt32("fillNumber", 8);
		item = new BMenuItem(("8"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 16
		message = new BMessage(MSG_FILL);
		message->AddInt32("note", note);
		message->AddInt32("fillNumber", 16);
		item = new BMenuItem(("16"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 32
		message = new BMessage(MSG_FILL);
		message->AddInt32("note", note);
		message->AddInt32("fillNumber", 32);
		item = new BMenuItem(("32"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
		// 64
		message = new BMessage(MSG_FILL);
		message->AddInt32("note", note);
		message->AddInt32("fillNumber", 64);
		item = new BMenuItem(("64"), message);
		item->SetTarget(this);
		fillMenu->AddItem(item);
	sampleMenu->AddItem(fillMenu);
	// END FILL MENu
	
	
	// CLEAR BUTTON
	message = new BMessage(MSG_CLEAR);
	message->AddInt32("note", note);
	item = new BMenuItem(("Clear"), message);
	item->SetTarget(this);
	sampleMenu->AddItem(item);
	
	// ADD ONE BELOW BUTTON
	message = new BMessage('ad1b');
	item = new BMenuItem(("Add one below"), message);
	item->SetTarget(this);
	sampleMenu->AddItem(item);
	
	// MENU methods
	where = ConvertToScreen(where);
	BRect mouseRect(where, where);
	mouseRect.InsetBy(-4.0, -4.0);
	sampleMenu->Go(where, true, false, mouseRect, true);
	
}
