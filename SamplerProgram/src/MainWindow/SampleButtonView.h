#ifndef SAMPLEBUTTONVIEW_H
#define SAMPLBBUTTONVIEW_H

#include <View.h>

#include "buttons/SampleButton.h"
#include "buttons/RecordButton.h"
//#include "buttons/DownArrowButton.h"
#include "buttons/TwoSymbolButton.h"
#include "buttons/Shapes.h"

class BGroupLayout;

class SampleButtonView:		public BView {
public:
						SampleButtonView(int32 id);
						~SampleButtonView();
						
		void				DisplayMenu(BPoint where, int32 id, SampleButton* button = NULL) const;
		SampleButton*		ButtonAt(int32 index) const;
		//TwoSymbolButton*	GroupButtonAt(int32 index) ;

						
private:
	BGroupLayout*	fButtonLayout;
	
	float				fSymbolHeight;
	int					fSampleHeight;
	Shapes*			fShapes;
	
	BShape*			fRecordShape;

	
	// BUTTONS

	//RecordButton*		fRecordButton;
	TwoSymbolButton*	fRecordButton;
	SampleButton*		fSampleButton;

};

#endif // _H
