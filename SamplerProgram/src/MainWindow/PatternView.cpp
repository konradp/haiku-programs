/* PatternView:	This is the area where we draw out dots (notes). We can draw max. 64 samples in a line (64 per sample).
				For now this number is hard-coded. The note positions are numbered 0-65 and stored in a list.
				In order to be able to draw a full dot (and not a half of it), the coordinates are shifted by number 8,
				which is also hard-coded for now	*/

#include <stdio.h>
#include "support/messages.h"
#include "PatternView.h"
#include <StorageKit.h>

#include <GridLayoutBuilder.h>
#include <GroupLayoutBuilder.h>
#include <LayoutBuilder.h>

PatternView::	PatternView(BList* samplesList)
//PatternView::	PatternView()
	//:	BView("Pattern Edit Frame", B_WILL_DRAW),
	:	BGroupView(B_HORIZONTAL, 0),
		fMessenger(NULL),
		//fResolution(3),			// no. of lines
		fRealResolution(8),		// real no. of lines 2^3=8
		fSampleHeight(24),		// sample height
		fSamplesNumber(8),		// no. of samples
		fSampleDuration(4),		// default note duration
		fWindowHorizShift(8)	//so that the whole node is drawn
{
	//fMatrix = groupMatrix;
	
	//fInstrumentView = new InstrumentView();	//ERROR
	fGView = new GView();	//ERROR
	fGView->SetExplicitMaxSize(BSize(70, 220));
		
	fEditView = new EditView(samplesList);
	
	//fEditView->SetTarget(this);
	
	//BGroupLayout* groupLayout;
	//groupLayout = new BGroupLayout(B_HORIZONTAL, 0);
	SetLayout(new BGroupLayout(B_HORIZONTAL, 0));
	
	AddChild(BGroupLayoutBuilder(B_HORIZONTAL, 0)
		.Add(BGroupLayoutBuilder(B_HORIZONTAL, 2)
			.Add(fGView)
			//.AddGlue()
			.Add(fEditView))
			//.AddGlue()
		//.AddGlue()
		.SetInsets(10, 0, 0, 0)
	);
	//roupLayout->AddView(fGView);
	//groupLayout->AddView(fEditView);
}

//Draw
void
PatternView::		Draw(BRect updateRect)
{
	fSampleWidth = (64*8)/fRealResolution;	// TODO: Move calculations out of DRAW()
}

// Message Received
void
PatternView::		MessageReceived(BMessage * msg)
{
	switch (msg->what)
	{
		case MSG_CLEAR:
		{
			printf("PatternView: Clear \n");
			fMessenger->SendMessage(msg);
			break;
		}
		
		case MSG_FILL:
		{
			printf("PatternView: Fill \n");
			fMessenger->SendMessage(msg);
			break;
		}
		
		case RESOLUTION_MSG:
		{
			printf("PatternView: Resize received \n");
			int32 size;
			if (msg->FindInt32("Resolution", &size) == B_OK) {
				//fResolution = size;
				fEditView->SetResolution(size);
			}
			break;
		}
		
		case MSG_ADD_NODE:
		{
			printf("PatternView: New node \n");
			fMessenger->SendMessage(msg);
			break;	
		}
		
		
		default:
		{
			BView::MessageReceived(msg);
			break;
		}	
	}
}

//SetSamplesNumber
void
PatternView::SetSamplesNumber(float number)
{fSamplesNumber = (int) number;}

void
PatternView::		SetTarget(BHandler* handler)
{
	delete fMessenger;
	fMessenger = new BMessenger(handler);
}

