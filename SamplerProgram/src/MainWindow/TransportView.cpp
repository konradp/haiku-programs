#include "TransportView.h"

#include <stdio.h>
#include <Button.h>
#include <GroupLayoutBuilder.h>
#include <LayoutBuilder.h>
#include <Shape.h>
#include <String.h>
#include <View.h>

#include "support/messages.h"

TransportView::TransportView()
	:	BView(BRect(0,0,1,1), "TransportView", B_FOLLOW_RIGHT, B_WILL_DRAW | B_NAVIGABLE_JUMP),
		fMessenger(NULL),
		fShapes(NULL),
		fPlayShape(NULL),
		fPauseShape(NULL)
{
	// Pick a symbol size based on the current system font size, but make
	// sure the size is uneven, so the pointy shapes have their middle on
	// a pixel instead of between two pixels.
	//fSymbolHeight = int(be_plain_font->Size() / 1.33) | 1;
	fSymbolHeight = int(be_plain_font->Size() / 2) | 1;
	CreateShapes();
	CreateViews();
}

TransportView::~TransportView()
{
}

void
TransportView::		CreateShapes()
{
	fSkipBackwardsShape = fShapes->SkipBackwardsShape(fSymbolHeight);
	fPlayShape = fShapes->PlayShape(fSymbolHeight);
	fPauseShape = fShapes->PauseShape(fSymbolHeight);
	fStopShape = fShapes->StopShape(fSymbolHeight);
	fSkipForwardShape = fShapes->SkipForwardShape(fSymbolHeight);
}

void
TransportView::		CreateViews()
{		
		// Buttons
		fSkipBackwardsButton = new SymbolButton(B_EMPTY_STRING,
			fSkipBackwardsShape, new BMessage(MSG_SKIP_BACKWARDS));
			
		fPlayPauseButton = new PlayPauseButton(B_EMPTY_STRING,
			fPlayShape, fPauseShape, new BMessage(MSG_PLAY_PAUSE));
			
		fStopButton = new SymbolButton(B_EMPTY_STRING,
			fStopShape, new BMessage(MSG_STOP));
		
		fSkipForwardButton = new SymbolButton(B_EMPTY_STRING,
			fSkipForwardShape, new BMessage(MSG_SKIP_FORWARD));
		
		SetLayout(new BGroupLayout(B_HORIZONTAL));
		
		AddChild(BGroupLayoutBuilder(B_HORIZONTAL, 0)
			.Add(fSkipBackwardsButton)
			.Add(fPlayPauseButton)
			.Add(fStopButton)
			.Add(fSkipForwardButton)
			.SetInsets(0, 0, 0, 0)
		);
}

//Message Received
void
TransportView::		MessageReceived(BMessage * msg)
{
	switch (msg->what)
	{	
		case MSG_PLAY_PAUSE:
		{
			printf("TransportView: Play button pressed \n");
			fMessenger->SendMessage(msg);
			break;
		}
		case MSG_STOP:
		{
			printf("TransportView: Stop button pressed \n");
			fMessenger->SendMessage(msg);
			break;
		}
		case B_QUIT_REQUESTED:	//This is not working. This is not done through the message, perhaps due to the flag passed to the view. Implement a QuitRequested() function instead?
		{
			printf("TransportView: Quitting! \n");
			break;
		}
		default:
		{
			printf("TransportView: Unknown MSG: \n");
			fMessenger->SendMessage(msg);
			break;
		}
	}
}

void
TransportView::		Play()
{
	printf("TransportView: Playing! \n");
}

void
TransportView::		SetTarget(BHandler* handler)
{
	delete fMessenger;
	fMessenger = new BMessenger(handler);
}
