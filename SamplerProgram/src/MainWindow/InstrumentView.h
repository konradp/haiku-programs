#ifndef InstrumentView_H
#define InstrumentView_H

#include <View.h>
#include <ObjectList.h>

#include <MediaFile.h>

#include <MidiProducer.h>
#include <MediaRoster.h>
#include <SoundPlayer.h>

#include "buttons/SampleButton.h"
#include "SampleButtonView.h"

#include <interface/Window.h>

#include "buttons/RecordButton.h"
//#include "buttons/Shapes.h"	// X

class BPopUpMenu;
class BGroupLayout;

class InstrumentView : public BView {
public:
						InstrumentView();
						~InstrumentView();
					
	virtual void		AttachedToWindow();
	virtual void		Draw(BRect updateRect);
	virtual void		MessageReceived(BMessage* message);
	//virtual void	MouseDown(BPoint point);

	SampleButton*		ButtonAt(int32 index) const;	// X
	void				SetSamplesNumber(int number);
		
private:
	int					fSamplesNumber;
	BList				fButtonList;
	BList				fSampleStatusList;	//A list of 0s (not playing) and 1s (playing)
		

	BGroupLayout*		fButtonLayout;
	//SampleButtonView*	fSampleButtonView;
	//TwoSymbolButton*	fDownArrowButton;
		
	Shapes*				fShapes;

	BShape*				fExpandGroupExpandedShape;
	BShape*				fExpandGroupCollapsedShape;
		
};

#endif // CHARTVIEW_H
