#ifndef TransportView_H
#define TransportView_H

#include <interface/Window.h>

#include "buttons/PlayPauseButton.h"
#include "buttons/Shapes.h"

class TransportView:	public BView {
public:
					TransportView();
					~TransportView();
	void			MessageReceived(BMessage* message);
	
	void			Play();
	void 			SetTarget(BHandler* handler);
								
private:
	void			CreateViews();
	void			CreateShapes();
	
	BMessenger*		fMessenger;
	BMessageRunner*	fMessageRunner;
	
	float			fSymbolHeight;
	
	// Shapes
	Shapes*			fShapes;
	BShape*			fSkipBackwardsShape;
	BShape*			fPlayShape;
	BShape*			fPauseShape;
	BShape*			fStopShape;
	BShape*			fSkipForwardShape;

	// Buttons
	PlayPauseButton*	fPlayPauseButton;
	SymbolButton*		fSkipBackwardsButton;
	SymbolButton*		fStopButton;
	SymbolButton*		fSkipForwardButton;
};

#endif // CHARTVIEW_H
