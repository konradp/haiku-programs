// TODO: Add MouseDown() for click-navigation
#include "TickView.h"

#include <stdio.h>
#include <MessageRunner.h>
#include <Messenger.h>
#include <View.h>

#include "support/messages.h"

TickView::TickView(BRect rect)
	:	BView(rect, "TickView",  B_FOLLOW_LEFT | B_FOLLOW_TOP, B_WILL_DRAW),
		fMessenger(NULL),
		fMessageRunner(NULL),
		fTickHeight(10.0),
		fTickOffset(105),	// FIX: this will change if the button size changes due to layout
		fTickPosition(0),
		fTickShape(NULL)
{
	printf("Ticker: constructor \n");
	fTickHeight = 10.0;
	fTickShape = new BShape();
	_CreateTickShape(fTickHeight);
}

TickView::~TickView()
{
}

void
TickView::		AttachedToWindow()
{
	SetDrawingMode(B_OP_BLEND);
}

void TickView::Draw(BRect updateRect)
{	
	//MovePenTo(BPoint(Bounds().LeftTop().x,0));
	//printf("TickView:: Draw() \n");
	//MovePenTo(BPoint(Bounds().LeftTop().x + fTickOffset,0));
	//MovePenTo(BPoint(Bounds().LeftTop().x,0));
	MovePenTo(BPoint(fTickPosition*8 + fTickOffset, 0));
	//int height = 10;
	FillShape(fTickShape);
}

BSize
TickView::MinSize()
{

	BSize size;
	size.width = 10;
	size.height = 10;
	//return BLayoutUtils::ComposeSize(ExplicitMinSize(), size);
	return size;
}

BSize
TickView::MaxSize()
{
	BSize size(MinSize());
	size.width = 20000;
	//return BLayoutUtils::ComposeSize(ExplicitMaxSize(), size);
	return size;
}

//Message Received
void
TickView::		MessageReceived(BMessage * msg)
{
	switch (msg->what)
	{		
		case MSG_TICK:	//REMOVE
		{	
			//printf("TickView:: TickMessage! \n");
			//int32 value;			
			//fTickOffset = msg->FindInt32("Tick", &value);
			//printf("offset: %d \n", fTickOffset);
			Invalidate();
			break;
		}
		default:
		{
			printf("TickView: Unknown MSG \n");
			break;
		}
	}
}

void
TickView::		SetPosition(int position)
{
	fTickPosition = position;
	//Invalidate();
}

void
TickView::		SetTarget(BHandler* handler)
{
	delete fMessenger;
	fMessenger = new BMessenger(handler);
}

// fSamplesAdded
void
TickView::		_CreateTickShape(float height)
{
	fTickShape->MoveTo(BPoint(0, 0));
	fTickShape->LineTo(BPoint(height, 0));
	fTickShape->LineTo(BPoint(height/2, height));
	fTickShape->Close();
}
