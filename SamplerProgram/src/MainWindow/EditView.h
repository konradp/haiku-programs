#ifndef EditView_H
#define EditView_H

#include <View.h>
#include <ObjectList.h>
#include "SampleTrack.h"


#include "support/messages.h"

class EditView:	public BView {
public:
					EditView(BList* samplesList);
					//EditView();
	virtual void	Draw(BRect updateRect);
	virtual void	MouseDown(BPoint where);
	virtual	void	SetResolution(int resolution);
	virtual	void	SetSamplesNumber(float number);
	BList			fNodeList;
	BList			fTimeList[64];
	BList*			fSamplesList;

private:
	int				fResolution;
	int				fRealResolution;
	int				fSampleHeight;
	int				fSampleWidth;
	int				fSamplesNumber;	
	int				fSampleDuration;
	int				fWindowHorizShift;
};

#endif // EditView_H
