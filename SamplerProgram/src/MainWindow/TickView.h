/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef TICKVIEW_H
#define TICKVIEW_H

#include <Shape.h>
#include <View.h>

class BMessageRunner;
class BMessenger;

class TickView:		public BView {
public:
					TickView(BRect frame);
					~TickView();
public:				
	virtual void	AttachedToWindow();								
	virtual void	Draw(BRect updateRect);
	void			MessageReceived(BMessage* message);
	void 			SetTarget(BHandler* handler);
public:
	virtual	BSize	MinSize();
	virtual	BSize	MaxSize();
public:
	void 			SetPosition(int position);
								
private:
	BMessenger*		fMessenger;
	BMessageRunner*	fMessageRunner;

	void			_CreateTickShape(float height);
	float			fTickHeight;
	int				fTickOffset;
	int				fTickPosition;
	BShape*			fTickShape;

};


#endif // TICKVIEW_H
