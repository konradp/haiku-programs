/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#include "SampleButton.h"

#include "../MainWindow/SampleButtonView.h"

#include <stdio.h>
#include <GradientLinear.h>
#include <LayoutUtils.h>
#include <Shape.h>
#include <Window.h>

// constructor
SampleButton::SampleButton(int id, const char* name, const char* label, BMessage* message)
	:
	BButton(name, label, message),
	id(id)
{
}

void
SampleButton::MouseDown(BPoint where)	//TODO: Clean up the logic here, callInherited? Do we need it?
{
	printf("SampleButton: MouseDown() \n");
	if (BMessage* message = Window()->CurrentMessage()) {
		printf("stage1 \n");
		uint32 buttons;
		message->FindInt32("buttons", (int32*)&buttons);
			if ((buttons & B_SECONDARY_MOUSE_BUTTON) != 0) {
				printf("stage2 \n");
				if (SampleButtonView* parent = dynamic_cast<SampleButtonView*>(Parent())) {	//FIX: Not working with Blayout???
					printf("stage3 \n");
					parent->DisplayMenu(where, id, this);
				}
			}
			else BButton::MouseDown(where);
	} else BButton::MouseDown(where);	
}

BSize
SampleButton::MinSize()
{
	return BButton::MinSize();
}

BSize
SampleButton::MaxSize()
{
	return BButton::MaxSize();
}
