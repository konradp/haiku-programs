/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef DownArrowButton_H
#define DownArrowButton_H

#include "SymbolButton.h"

#include <ControlLook.h>


class DownArrowButton : public SymbolButton {
public:
					DownArrowButton(int32 id,
						const char* name,
						BShape* downArrowShape,
						BShape* downArrowCollapsedShape,
						BMessage* message = NULL);
							
	// BButton interface
	virtual	void	Draw(BRect updateRect);	
	virtual	BSize	MinSize();
	virtual	BSize	MaxSize();
	
	bool			fEnabled;
			
private:
	BShape*			fDownArrowShape;
	BShape*			fDownArrowCollapsedShape;
	enum {
					kStopped = 0,
					kPaused,
					kPlaying
	};
	uint32			fPlaybackState;
	uint32			fBorders;
};


#endif // DownArrowButton_H
