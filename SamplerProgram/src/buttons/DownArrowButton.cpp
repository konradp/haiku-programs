#include "DownArrowButton.h"

#include <stdio.h>
#include <GradientLinear.h>
#include <Shape.h>
#include <LayoutUtils.h>


// constructor
DownArrowButton::DownArrowButton(int32 id, const char* name, BShape* downArrowShape, BShape* downArrowCollapsedShape, BMessage* message)
	:
	SymbolButton(name, NULL, message),
	fEnabled(true),
	fDownArrowShape(downArrowShape),
	fDownArrowCollapsedShape(downArrowCollapsedShape)
{
	//printf("DownArrowButton: constructor \n");
}

void
DownArrowButton::Draw(BRect updateRect)
{
	SymbolButton::Draw(updateRect);

	rgb_color base = ui_color(B_PANEL_BACKGROUND_COLOR);
	//rgb_color active = (rgb_color){ 116, 224, 0, 255 };

	BRect bounds(Bounds());
	//BRect pauseBounds = fDownArrowShape->Bounds();

	BPoint offset;
	offset.x = (bounds.left + bounds.right) / 2;
	offset.y = (bounds.top + bounds.bottom) / 2;
	offset.x -= fDownArrowShape->Bounds().Width() / 2;
	offset.y -= fDownArrowShape->Bounds().Height() / 2;
	offset.x = floorf(offset.x - fDownArrowShape->Bounds().left);
	offset.y = ceilf(offset.y - fDownArrowShape->Bounds().top);
	
	//BPoint offset(0,0);

	MovePenTo(offset);
	offset.y += fDownArrowShape->Bounds().Height();
	if ( fEnabled == true )
		FillShape(fDownArrowShape);
	else
		FillShape(fDownArrowCollapsedShape);
}

BSize
DownArrowButton::MinSize()
{
	if (fDownArrowShape == NULL)
		return BButton::MinSize();

	float scale = fBorders != 0 ? 2.5f : 1.0f;
	
	BSize size;
	size.width = ceilf(fDownArrowShape->Bounds().Width() * scale);
	size.height = ceilf(fDownArrowShape->Bounds().Height() * scale+2);
	return BLayoutUtils::ComposeSize(ExplicitMinSize(), size);
}


BSize
DownArrowButton::MaxSize()
{
	BSize size(MinSize());
	if (fBorders != 0)
		size.width = ceilf(size.width * 1.5f);	//check
	return BLayoutUtils::ComposeSize(ExplicitMaxSize(), size);
}
