#include "RecordButton.h"

#include <stdio.h>
#include <GradientLinear.h>
#include <Shape.h>
#include <LayoutUtils.h>


// constructor
RecordButton::RecordButton(int32 id, const char* name, BShape* recordSymbolShape, BMessage* message)
	:
	SymbolButton(name, NULL, message),
	fRecordSymbol(recordSymbolShape)
{
	//printf("RecordButton: constructor \n");
}

void
RecordButton::Draw(BRect updateRect)
{
	SymbolButton::Draw(updateRect);

	rgb_color base = ui_color(B_PANEL_BACKGROUND_COLOR);
	//rgb_color active = (rgb_color){ 116, 224, 0, 255 };

	BRect bounds(Bounds());
	//BRect pauseBounds = fRecordSymbol->Bounds();

	BPoint offset;
	offset.x = (bounds.left + bounds.right) / 2;
	offset.y = (bounds.top + bounds.bottom) / 2;
	offset.x -= fRecordSymbol->Bounds().Width() / 2;
	offset.y -= fRecordSymbol->Bounds().Height() / 2;
	offset.x = floorf(offset.x - fRecordSymbol->Bounds().left);
	offset.y = ceilf(offset.y - fRecordSymbol->Bounds().top);
	
	//BPoint offset(0,0);

	MovePenTo(offset);
	offset.y += fRecordSymbol->Bounds().Height();
	FillShape(fRecordSymbol);
}

BSize
RecordButton::MinSize()
{
	if (fRecordSymbol == NULL)
		return BButton::MinSize();

	float scale = fBorders != 0 ? 2.5f : 1.0f;
	
	BSize size;
	size.width = ceilf(fRecordSymbol->Bounds().Width() * scale);
	size.height = ceilf(fRecordSymbol->Bounds().Height() * scale+2);
	return BLayoutUtils::ComposeSize(ExplicitMinSize(), size);
}


BSize
RecordButton::MaxSize()
{
	BSize size(MinSize());
	if (fBorders != 0)
		size.width = ceilf(size.width * 1.5f);	//check
	return BLayoutUtils::ComposeSize(ExplicitMaxSize(), size);
}
