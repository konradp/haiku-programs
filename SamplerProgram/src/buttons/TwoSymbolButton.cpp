/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#include "TwoSymbolButton.h"

#include <stdio.h>
#include <GradientLinear.h>
#include <LayoutUtils.h>
#include <Shape.h>

// constructor
TwoSymbolButton::TwoSymbolButton(int32 id, const char* name, BShape* onSymbolShape, BShape* offSymbolShape, BMessage* message, uint32 borders)
	:
	BButton(name, NULL, message),
	fOnShape(onSymbolShape),
	fOffShape(offSymbolShape),
	fBorders(borders)
{
}

void
TwoSymbolButton::Draw(BRect updateRect)
{
	uint32 flags = be_control_look->Flags(this);
	rgb_color base = LowColor();
	BRect bounds(Bounds());

	if (fBorders != 0) {
		be_control_look->DrawButtonFrame(this, bounds, updateRect, base,
			base, flags & ~BControlLook::B_DISABLED, fBorders);
		be_control_look->DrawButtonBackground(this, bounds, updateRect, base,
			flags);
	} else
		FillRect(updateRect, B_SOLID_LOW);

	if (fOnShape == NULL || fOffShape == NULL)
		return;

	if (IsEnabled()) {
		if (Value() == B_CONTROL_ON)
			base = tint_color(base, (B_DARKEN_4_TINT + B_DARKEN_MAX_TINT) / 2);
		else
			base = tint_color(base, B_DARKEN_4_TINT);
	} else {
		if (Value() == B_CONTROL_ON)
			base = tint_color(base, B_DARKEN_2_TINT);
		else
			base = tint_color(base, B_DARKEN_1_TINT);
	}

	BPoint offset;
	offset.x = (bounds.left + bounds.right) / 2;
	offset.y = (bounds.top + bounds.bottom) / 2;
	offset.x -= fOnShape->Bounds().Width() / 2;
	offset.y -= fOnShape->Bounds().Height() / 2;
	offset.x = floorf(offset.x - fOnShape->Bounds().left);
	offset.y = ceilf(offset.y - fOnShape->Bounds().top);

	MovePenTo(offset);
	BGradientLinear gradient;
	gradient.AddColor(tint_color(base, B_DARKEN_1_TINT), 0);
	gradient.AddColor(base, 255);
	gradient.SetStart(offset);
	offset.y += fOnShape->Bounds().Height();
	gradient.SetEnd(offset);
	
	if ( fEnabled == true )
		FillShape(fOnShape);
	else
		FillShape(fOffShape);
	//FillShape(fOnSymbol, gradient);
}

BSize
TwoSymbolButton::MinSize()
{
	if (fOnShape == NULL)
		return BButton::MinSize();

	float scale = fBorders != 0 ? 2.5f : 1.0f;

	BSize size;
	size.width = ceilf(fOnShape->Bounds().Width() * scale);
	size.height = ceilf(fOnShape->Bounds().Height() * scale);
	return BLayoutUtils::ComposeSize(ExplicitMinSize(), size);
}


BSize
TwoSymbolButton::MaxSize()
{
	BSize size(MinSize());
	if (fBorders != 0)
		size.width = ceilf(size.width * 1.5f);
	return BLayoutUtils::ComposeSize(ExplicitMaxSize(), size);
}
