/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef RecordButton_H
#define RecordButton_H

#include "SymbolButton.h"

#include <ControlLook.h>


class RecordButton : public SymbolButton {
public:
					RecordButton(int32 id,
						const char* name,
						BShape* recordSymbolShape,
						BMessage* message = NULL);
							
	// BButton interface
	virtual	void	Draw(BRect updateRect);	
	virtual	BSize	MinSize();
	virtual	BSize	MaxSize();
			
private:
	BShape*			fRecordSymbol;
	enum {
					kStopped = 0,
					kPaused,
					kPlaying
	};
	uint32			fPlaybackState;
	uint32			fBorders;
};


#endif // RecordButton_H
