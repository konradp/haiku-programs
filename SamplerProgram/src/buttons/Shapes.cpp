/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#include "Shapes.h"

Shapes::	Shapes()
{	
}

BShape*
Shapes::	PlayShape(float height)
{
	BShape* shape = new BShape();

	float step = floorf(height / 8);

	shape->MoveTo(BPoint(height + step, height / 2));
	shape->LineTo(BPoint(-step, height + step));
	shape->LineTo(BPoint(-step, 0 - step));
	shape->Close();

	return shape;
}

BShape*
Shapes::	PauseShape(float height)
{
	BShape* shape = new BShape();

	float stemWidth = floorf(height / 3);

	shape->MoveTo(BPoint(0, height));
	shape->LineTo(BPoint(stemWidth, height));
	shape->LineTo(BPoint(stemWidth, 0));
	shape->LineTo(BPoint(0, 0));
	shape->Close();

	shape->MoveTo(BPoint(height - stemWidth, height));
	shape->LineTo(BPoint(height, height));
	shape->LineTo(BPoint(height, 0));
	shape->LineTo(BPoint(height - stemWidth, 0));
	shape->Close();

	return shape;
}

BShape*
Shapes::	StopShape(float height)
{
	BShape* shape = new BShape();

	shape->MoveTo(BPoint(0, height));
	shape->LineTo(BPoint(height, height));
	shape->LineTo(BPoint(height, 0));
	shape->LineTo(BPoint(0, 0));
	shape->Close();

	return shape;
}

BShape*
Shapes::	RecordShape(float height)
{
	BShape* shape = new BShape();

	/*shape->MoveTo(BPoint(0, height));
	shape->LineTo(BPoint(height, height));
	shape->LineTo(BPoint(height, 0));
	shape->LineTo(BPoint(0, 0));
	shape->Close();*/
	shape->MoveTo(BPoint(0, height/2));
	shape->ArcTo(height/2, height/2, 0, true, false, BPoint(height/2, height/2));
	//shape->ArcTo(height/2, height/2, 0, true, true, BPoint(0, 0));
	shape->Close();
	

	return shape;
}

BShape*
Shapes::	SkipBackwardsShape(float height)
{
	BShape* shape = new BShape();

	float stopWidth = ceilf(height / 6);

	shape->MoveTo(BPoint(-stopWidth, height));
	shape->LineTo(BPoint(0, height));
	shape->LineTo(BPoint(0, 0));
	shape->LineTo(BPoint(-stopWidth, 0));
	shape->Close();

	shape->MoveTo(BPoint(0, height / 2));
	shape->LineTo(BPoint(height, height));
	shape->LineTo(BPoint(height, 0));
	shape->Close();

	shape->MoveTo(BPoint(height, height / 2));
	shape->LineTo(BPoint(height * 2, height));
	shape->LineTo(BPoint(height * 2, 0));
	shape->Close();

	return shape;
}


BShape*
Shapes::	SkipForwardShape(float height)
{
	BShape* shape = new BShape();

	shape->MoveTo(BPoint(height, height / 2));
	shape->LineTo(BPoint(0, height));
	shape->LineTo(BPoint(0, 0));
	shape->Close();

	shape->MoveTo(BPoint(height * 2, height / 2));
	shape->LineTo(BPoint(height, height));
	shape->LineTo(BPoint(height, 0));
	shape->Close();

	float stopWidth = ceilf(height / 6);

	shape->MoveTo(BPoint(height * 2, height));
	shape->LineTo(BPoint(height * 2 + stopWidth, height));
	shape->LineTo(BPoint(height * 2 + stopWidth, 0));
	shape->LineTo(BPoint(height * 2, 0));
	shape->Close();

	return shape;
}

BShape*
Shapes::	ExpandGroupExpandedShape(float height)
{
	BShape* shape = new BShape();

	shape->MoveTo(BPoint(0, 0));
	shape->LineTo(BPoint(height/2, 3*height/4));
	shape->LineTo(BPoint(height, 0));
	shape->Close();

	return shape;
}

BShape*
Shapes::	ExpandGroupCollapsedShape(float height)
{
	BShape* shape = new BShape();

	shape->MoveTo(BPoint(0, height/4));
	shape->LineTo(BPoint(0, height/2));
	shape->LineTo(BPoint(height, height/2));
	shape->LineTo(BPoint(height, height/4));
	shape->Close();

	return shape;
}
