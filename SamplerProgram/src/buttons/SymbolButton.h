/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef SYMBOLBUTTON_H
#define SYMBOLBUTTON_H

#include <Button.h>
#include <ControlLook.h>


class SymbolButton : public BButton {
public:
					SymbolButton(const char* name,
						BShape* symbolShape,
						BMessage* message = NULL,
						uint32 borders
							= BControlLook::B_ALL_BORDERS);
				
	virtual	void	Draw(BRect updateRect);
	virtual	BSize	MinSize();
	virtual	BSize	MaxSize();
								
private:
	BShape*				fSymbol;
	uint32				fBorders;

};


#endif // _H
