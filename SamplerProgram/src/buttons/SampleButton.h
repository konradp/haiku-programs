/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef SampleButton_H
#define SampleButton_H

#include <Button.h>
#include <ControlLook.h>


class SampleButton : public BButton {
public:
					SampleButton(int id,
						const char* name,
						const char* label = "blaa",
						BMessage* message = NULL);
				
	virtual	void	MouseDown(BPoint where);
	
	virtual	BSize	MinSize();
	virtual	BSize	MaxSize();
								
private:
int32 id;

};


#endif // _H
