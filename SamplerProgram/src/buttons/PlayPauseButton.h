/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef PLAYPAUSEBUTTON_H
#define PLAYPAUSEBUTTON_H

#include "SymbolButton.h"

#include <ControlLook.h>


class PlayPauseButton : public SymbolButton {
public:
					PlayPauseButton(const char* name,
						BShape* playSymbolShape,
						BShape* pauseSymbolShape,
						BMessage* message = NULL);
							
	// BButton interface
	virtual	void	Draw(BRect updateRect);	
	virtual	BSize	MinSize();
	virtual	BSize	MaxSize();
			
private:
	BShape*			fPlaySymbol;
	BShape*			fPauseSymbol;
	enum {
					kStopped = 0,
					kPaused,
					kPlaying
	};
	uint32			fPlaybackState;
			
};


#endif // PLAYPAUSEBUTTON_H
