/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef TwoSymbolButton_H
#define TwoSymbolButton_H

#include <Button.h>
#include <ControlLook.h>


class TwoSymbolButton : public BButton {
public:
					TwoSymbolButton(int32 id, const char* name,
						BShape* onSymbolShape,
						BShape* offSymbolShape,
						BMessage* message = NULL,
						uint32 borders
							= BControlLook::B_ALL_BORDERS);
				
	virtual	void	Draw(BRect updateRect);
	virtual	BSize	MinSize();
	virtual	BSize	MaxSize();
	
	bool			fEnabled;
					
private:
	BShape*				fOnShape;
	BShape*				fOffShape;
	uint32				fBorders;

};


#endif // _H
