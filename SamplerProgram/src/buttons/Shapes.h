/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef SHAPES_H
#define SHAPES_H

#include <Shape.h>
#include <SupportDefs.h>


class Shapes
{
public:
			Shapes();
			
	BShape*	SkipBackwardsShape(float height);
	BShape*	PlayShape(float height);
	BShape*	PauseShape(float height);
	BShape*	StopShape(float height);
	BShape*	RecordShape(float height);
	BShape*	SkipForwardShape(float height);
	
	// SampleButtonView
	BShape* ExpandGroupExpandedShape(float height);
	BShape* ExpandGroupCollapsedShape(float height);
	
								
private:

};


#endif // _H
