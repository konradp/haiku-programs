/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef AUDIOMIXERVIEW_H
#define AUDIOMIXERVIEW_H

#include <View.h>

class BMessageRunner;
class BMessenger;

class AudioMixerView : public BView {
public:
					AudioMixerView(BRect rect);
	virtual			~AudioMixerView();
	
	virtual void	AttachedToWindow();								
	virtual void	Draw(BRect updateRect);
	virtual void	MessageReceived(BMessage* message);
	void 			SetTarget(BHandler* handler);
								
private:
	BMessenger*		fMessenger;
	BMessageRunner*	fMessageRunner;

};


#endif // _H
