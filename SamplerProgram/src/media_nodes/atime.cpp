#include "atime.h"
#include <stdio.h>

//=======================================================
// 	Class: ATime
//	A thread that provides a tick event on a timed basis
//=======================================================

ATime::	ATime()
	: 	fSeconds(0.0)
{
}

ATime::	ATime(const double aValue)
	:	fSeconds(aValue)
{
}

ATime::	ATime(const ATime& other)
	: 	fSeconds(other.fSeconds)
{
}

ATime::~ATime()
{}

ATime&
ATime::	operator=(const ATime& other)
{
	fSeconds = other.fSeconds;
	return *this;
}

ATime&
ATime::	operator-(const ATime& other)
{
	fSeconds -= other.fSeconds;
	return *this;
}

void
ATime::	SetSeconds(const double secs)
{
	fSeconds = secs;
}

const double
ATime::	GetSeconds() const
{
	return fSeconds;
}

static long
ATicker_thread(void *data)
{
	ATicker *ticker = (ATicker*)data;
	double nextTime;
	double snoozeTime;
	double baseTime;
	double tickCount = 0;
	microseconds tickInterval;
	
	
	
	baseTime = system_time();
	
	// Run continuously.  The only thing that stops this
	// is the death of the thread.
	while(1)
	{
		ticker->GetInterval(tickInterval);
		nextTime = baseTime + tickInterval;
		snoozeTime = (nextTime - system_time());
		
		while (snoozeTime >= 1.0) {
			snooze(snoozeTime);
			snoozeTime = nextTime - system_time();
		}
		
		tickCount++;
		ticker->Tick(tickCount);
		
		baseTime = nextTime;
	}
	return 0;
}

ATicker::ATicker(ATime &tickInterval)
	: fInterval(tickInterval)
{
	fThreadID = spawn_thread(ATicker_thread,
		"ticker",B_NORMAL_PRIORITY,this);
	resume_thread(fThreadID);
}

ATicker::ATicker(const double tickRate)
{
	fInterval = seconds(1.0 / tickRate);
	
	// Number of times to tick a second
	fThreadID = spawn_thread(ATicker_thread,
		"ticker",B_NORMAL_PRIORITY,this);
	resume_thread(fThreadID);
}			

ATicker::~ATicker()
{
	// kill the thread
	kill_thread(fThreadID);
}
		
void	
ATicker::Tick(const double tickCount)
{
	printf("ATime::Tick: %f\n", tickCount);
}

void
ATicker::GetInterval(ATime &interval)
{
	interval = fInterval;
}

void
ATicker::	SetInterval(const double tickRate)
{
	printf("ATicker: tickRate: %f \n", tickRate);
	fInterval = seconds(1.0 / tickRate );
	printf("ATicker: fInterval: %f \n", (double) fInterval);
}
