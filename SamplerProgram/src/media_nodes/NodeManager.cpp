#include "NodeManager.h"

#include <stdio.h>
#include <MediaRoster.h>
#include <StorageKit.h>			//For BPath
#include <TimeSource.h>

#include "support/messages.h"
#include "support/wavDefs.h"	//for wav header and structs
#include <stdio.h>
#include <stdlib.h>				//for ErrorCheck's strerror



NodeManager::	NodeManager()
	:
	fRoster(NULL)
{
	printf("NodeManager: Constructor() \n");
	status_t err;
	err = _SetUpNodes();
	if ( err != B_OK ) {
		printf("NodeManager: Constructor: _SetUpNodes failed \n");	
	}
}


NodeManager::	~NodeManager()
{
	_TearDownNodes();
}




status_t
NodeManager::	_SetUpNodes()
{
	printf("NodeManager: _SetUpNodes() \n");
	status_t err = B_OK;
	printf("NodeManager: Getting time source...\n");
	fRoster->GetTimeSource(&fTimeSource);
	
	err = _SetUpMonoSynth();
	if ( err != B_OK ) {
		printf("NodeManager: _SetUpNodes(): Could not set up the MonoSynth \n");
		return err;
	}
	err = _SetUpRecorder();
	if ( err != B_OK ) {
		printf("NodeManager: _SetUpNodes(): Failed to set up recorder \n");
		return err;
	}
	return err;
}


// MONO SYNTH
status_t
NodeManager::	_SetUpMonoSynth()
{	
	printf("NodeManager: _SetUpMonoSynth() \n");
	fStatus = B_OK;
	
	// TONE PRODUCER ******************
	// TONE PRODUCER	//this needs to be in the constructor, otherwise crashes
	fMonoSynth = new MonoSynth();
	
	//printf("NodeManager: Registering MonoSynth...\n");
	fStatus = fRoster->RegisterNode(fMonoSynth);
	if (fStatus != B_OK) {
		printf("Unable to register fBlankProducer node!\n");
		return fStatus;
	}
	// making sure Media Roster knows we're using the node
	fRoster->GetNodeFor(fMonoSynth->Node().node, &fMonoSynthConnection.producer);
	fRoster = BMediaRoster::Roster();
	
	//printf("NodeManager: Getting mixer...\n");
	fStatus = fRoster->GetAudioMixer(&fMonoSynthConnection.consumer);	//assign audio mixer to fAudioMixer, return error
	if (fStatus != B_OK) {
		printf("unable to get the system mixer");
		return fStatus;
	}
			
	//printf("NodeManager: SETTING time source...\n");
	fRoster->SetTimeSourceFor(fMonoSynthConnection.producer.node, fTimeSource.node);
	
	//printf("NodeManager: Got nodes. Finding endpoints...\n");
	media_input mixerInput;
	media_output soundOutput;
	int32 count = 1;

	//printf("NodeManager: Getting free outputs for producer...\n");
	fStatus = fRoster->GetFreeOutputsFor(fMonoSynthConnection.producer, &soundOutput, 1, &count);
	if (fStatus != B_OK) {
		printf("unable to get a free output from the producer node");
		return fStatus;
	}
	count = 1;
	//printf("NodeManager: Getting free inputs for consumer...\n");
	fStatus = fRoster->GetFreeInputsFor(fMonoSynthConnection.consumer, &mixerInput, 1, &count);
	if (fStatus != B_OK) {
		printf("unable to get a free input to the mixer");
		return fStatus;
	}
			
	// got the endpoints, not connect it
	//printf("NodeManager: Connecting outputs...\n");
	media_format format;
	format.type = B_MEDIA_RAW_AUDIO;
	format.u.raw_audio = media_raw_audio_format::wildcard;
	fStatus = fRoster->Connect(soundOutput.source, mixerInput.destination, &format, &soundOutput, &mixerInput);
	if (fStatus != B_OK) {
		printf("unable to connect nodes");
		return fStatus;
	}
				
	//printf("NodeManager: Setting format...\n");
	fMonoSynthConnection.format = format;
	fMonoSynthConnection.source = soundOutput.source;
	fMonoSynthConnection.destination = mixerInput.destination;
			
	//printf("MinWindow: Setting runMode...\n");
	fRoster->SetRunModeNode(fMonoSynthConnection.producer, BMediaNode::B_INCREASE_LATENCY);
	return fStatus;
}


status_t
NodeManager::_StartMonoSynth()
{
	// Time Source
	printf("Making time source...\n");
	BTimeSource* timeSource = fRoster->MakeTimeSourceFor(fMonoSynthConnection.producer);
	if(!timeSource)
	printf("ERROR - MakeTimeSourceFor(producer) returned NULL!\n");

	printf("Getting latency for producer...\n");
	bigtime_t latency = 0;
	fRoster->GetLatencyFor(fMonoSynthConnection.producer, &latency);
	printf("Starting node! \n");
	//the StartNode will crash if there's no node, include a B_OK check
	fRoster->StartNode(fMonoSynthConnection.producer, timeSource->Now() + latency);
	timeSource->Release();
	return fStatus;
}

void
NodeManager::_StopMonoSynth()
{
	BMediaRoster* r = BMediaRoster::Roster();
	r->StopNode(fMonoSynthConnection.producer, 0, true);		// synchronous stop
}

status_t
NodeManager::_TearDownNodes()
{
	status_t err = B_OK;	// TODO: Do we need this here?
	
	// Disconnect nodes from Roster: fails if not connected!
	err = fRoster->Disconnect(fMonoSynthConnection.producer.node, fMonoSynthConnection.source,
		fMonoSynthConnection.consumer.node, fMonoSynthConnection.destination);
	if (err)
		printf("Error disconnecting nodes \n");

	// Release nodes
	//printf("NodeManager: Releasing nodes \n");
	fRoster->ReleaseNode(fMonoSynthConnection.producer);
	fRoster->ReleaseNode(fMonoSynthConnection.consumer);
	return err;
}

// RECORDER
status_t
NodeManager::	_SetUpRecorder()
{
	printf("NodeManager: _SetUpRecorder() \n");
	fRecording = false;
	status_t err = B_OK;
	
	//	Find temp directory for recorded sounds
	BPath path;
	if ( !(err = find_directory(B_SYSTEM_TEMP_DIRECTORY, &path)) )
		err = fTempDir.SetTo(path.Path());
	if (err < 0) {
		printf("ERROR: Recorder: Can't find temporary directory for recorded sounds. \n");
		// TODO: set bool false, add if checks later on
	}
		
	// RECORDER
	fRecorder = new BMediaRecorder("Sound Recorder", B_MEDIA_RAW_AUDIO);

	if (fRecorder->InitCheck() != B_OK)
		printf("ERROR: Recorder: InitCheck() returned error! \n");

	// Set the node to accept only audio data
	media_format output_format;
	output_format.type = B_MEDIA_RAW_AUDIO;
	output_format.u.raw_audio = media_raw_audio_format::wildcard;
	fRecorder->SetAcceptedFormat(output_format);
		
	err = fRoster->GetAudioInput(&fAudioInputNode);
	if (err != B_OK) {
		printf("NodeManager: _SetUpRecorder: Can't get audio input \n");
		return err;
	}
	return err;
}


void
NodeManager::_Record(BMessage * message)
{
	printf("NodeManager: _Record \n");
	//	User pressed Record button
	fRecording = true;
	//if (fButtonState != btnPaused) {
	//	StopRecording();
	//	return;			//	user is too fast on the mouse
	//}
	//SetButtonState(btnRecording);
	//fRecordButton->SetRecording();

	char name[256];
	//	Create a file with a temporary name
	status_t fStatus = _NewTempRecName(name);
	if (fStatus != B_OK) {
		printf("NodeManager: _Record: Cannot find an unused name to use for the new recording");
		return;
	}
	//	Find temp file for future reference
	fStatus = fTempDir.FindEntry(name, &fRecEntry);
	if (fStatus != B_OK) {
		printf("NodeManager: _Record: Cannot find the temporary file created to hold the new recording");
		return;
	}
	// Open temp file
	fStatus = fRecFile.SetTo(&fTempDir, name, O_RDWR);
	if (fStatus != B_OK) {
		printf("NodeManager: _Record: Cannot open the temporary file created for the new recording");
		fRecEntry.Unset();
		return;
	}
	//	Reserve space on disk (creates fewer fragments)
	fStatus = fRecFile.SetSize(4 * fRecordFormat.u.raw_audio.channel_count
		* fRecordFormat.u.raw_audio.frame_rate
		* (fRecordFormat.u.raw_audio.format
			& media_raw_audio_format::B_AUDIO_SIZE_MASK));
	if (fStatus != B_OK) {
		printf("NodeManager: _Record: The recording is too long");
		fRecEntry.Remove();
		fRecEntry.Unset();
		return;
	}
	fRecSize = 0;
	fRecFile.Seek(sizeof(struct wav_struct), SEEK_SET);

	//	Hook up input
	fStatus = _MakeRecordConnection(fAudioInputNode);	//make this different than the default node, e.g. the toneproducer
	if (fStatus != B_OK) {
		printf("NodeManager: _Record: Cannot connect to the selected sound input \n");
		fRecEntry.Remove();
		fRecEntry.Unset();
		return;
	} //endif

	fRecorder->Start();
}


status_t
NodeManager::_NewTempRecName(char * name)
{
	int init_count = fTempRecCount;
again:	//label
	if (fTempRecCount-init_count > 25) {
		return B_ERROR;
	}
	else {
		fTempRecCount++;
		if (fTempRecCount==0)
			sprintf(name, "Audio Clip");
		else
			sprintf(name, "Audio Clip %d", fTempRecCount);
		BPath path;

		BEntry tempEnt;
		if ((fStatus = fTempDir.GetEntry(&tempEnt)) != B_OK)
			return fStatus;
		if ((fStatus = tempEnt.GetPath(&path)) != B_OK)
			return fStatus;
		path.Append(name);
		int fd;
		//	Use O_EXCL so we know we created the file (sync with other instances)
		if ((fd = open(path.Path(), O_RDWR | O_CREAT | O_EXCL, 0666)) < 0)
			goto again;
		close(fd);
	}
	return B_OK;
}


status_t
NodeManager::_MakeRecordConnection(const media_node & input)
{
	printf("Recorder::MakeRecordConnection()\n");

	status_t err = B_OK;
	media_output audioOutput;

	if (!fRecorder->IsConnected()) {
		//	Find an available output for the given input node.
		int32 count = 0;
		err = fRoster->GetFreeOutputsFor(input, &audioOutput, 1, &count, B_MEDIA_RAW_AUDIO);
		if (err < B_OK) {
			printf("RecorderWindow::MakeRecordConnection(): couldn't get free outputs from audio input node\n");
			return err;
		}
		if (count < 1) {
			printf("RecorderWindow::MakeRecordConnection(): no free outputs from audio input node\n");
			return B_BUSY;
		}
	} else {
		printf("Recorder::MakeRecordConnection(): audio input node already connected\n");
		return B_BUSY;
	}

	//	Get a format, any format.
	fRecordFormat.u.raw_audio = audioOutput.format.u.raw_audio;
	fRecordFormat.type = B_MEDIA_RAW_AUDIO;

	//	Tell the consumer where we want data to go.
	err = fRecorder->SetHooks(_RecordFile, _NotifyRecordFile, this);
	if (err < B_OK) {
		printf("Recorder::MakeRecordConnection(): couldn't set the sound recorder's hook functions\n");
		return err;
	}

	if (!fRecorder->IsConnected()) {
		err = fRecorder->Connect(input, &audioOutput, &fRecordFormat);
		if (err < B_OK) {
			printf("RecorderWindow::MakeRecordConnection(): failed to connect sound recorder to audio input node.\n");
			fRecorder->SetHooks(NULL, NULL, NULL);
			return err;
		}
	}
	return B_OK;
}


void
NodeManager::_RecordFile(void* cookie, bigtime_t timestamp,
	void* data, size_t size, const media_format &format)
{
	//	Callback called from the SoundConsumer when receiving buffers.
	NodeManager * window = (NodeManager *)cookie;

	if (window->fRecording) {
		//	Write the data to file (we don't buffer or guard file access
		//	or anything)
		window->fRecFile.WriteAt(window->fRecSize, data, size);
		//window->fVUView->ComputeLevels(data, size, format.u.raw_audio.format);
		window->fRecSize += size;
	}
}


void
NodeManager::_NotifyRecordFile(void * cookie, BMediaRecorder::notification code, ...)
{
	if (code == BMediaRecorder::B_WILL_STOP) {
		NodeManager * window = (NodeManager *)cookie;
		// Tell the window we've stopped, if it doesn't
		// already know.
		window->PostMessage(MSG_STOP_RECORDING);
	} //if
}


status_t
NodeManager::_BreakRecordConnection()
{
	status_t err = B_OK;
	err = fRecorder->Stop(true);
	if (err < B_OK)
		return err;
	return fRecorder->Disconnect();
}


status_t
NodeManager::_StopRecording()
{
	if (!fRecording)
		return B_OK;
	fRecording = false;

	_BreakRecordConnection();
	fRecorder->SetHooks(NULL, NULL, NULL);

	if (fRecSize > 0) {
		wav_struct header;
		header.riff.riff_id = FOURCC('R','I','F','F');
		header.riff.len = fRecSize + sizeof(header) - 8;
		header.riff.wave_id = FOURCC('W','A','V','E');
		header.format_chunk.fourcc = FOURCC('f','m','t',' ');
		header.format_chunk.len = sizeof(header.format);
		header.format.format_tag = 1;
		header.format.channels = fRecordFormat.u.raw_audio.channel_count;
		header.format.samples_per_sec = (uint32)fRecordFormat.u.raw_audio.frame_rate;
		header.format.avg_bytes_per_sec = (uint32)(fRecordFormat.u.raw_audio.frame_rate
			* fRecordFormat.u.raw_audio.channel_count
			* (fRecordFormat.u.raw_audio.format & 0xf));
		header.format.bits_per_sample = (fRecordFormat.u.raw_audio.format & 0xf) * 8;
		header.format.block_align = (fRecordFormat.u.raw_audio.format & 0xf)
			* fRecordFormat.u.raw_audio.channel_count;
		header.data_chunk.fourcc = FOURCC('d','a','t','a');
		header.data_chunk.len = fRecSize;
		fRecFile.Seek(0, SEEK_SET);
		fRecFile.Write(&header, sizeof(header));

		fRecFile.SetSize(fRecSize + sizeof(header));
		//	We reserve space; make sure we cut off any excess at the end.
		//AddSoundItem(fRecEntry, true);
	} else
		fRecEntry.Remove();
	//	We're done for this time.
	fRecEntry.Unset();
	//	Close the file.
	fRecFile.Unset();
	//	No more recording going on.
	fRecSize = 0;
	//SetButtonState(btnPaused);
	//fRecordButton->SetStopped();
	return B_OK;
}


void			//MESSAGE_RECEIVED
NodeManager::	MessageReceived(BMessage * msg)
{
	switch (msg->what)
	{
		case MSG_RECORD:
		{
			printf("NodeManager: MSG_RECORD received \n");
			break;
		}
		case 'blaa':
		{
			printf("NodeManager: blaa received \n");
			break;
		}
		default:
		{
			printf("NodeManager: Unknown message \n");
			break;
		}
	}
}

void
NodeManager::		SetTarget(BHandler* handler)
{
	delete fMessenger;
	fMessenger = new BMessenger(handler);
}
