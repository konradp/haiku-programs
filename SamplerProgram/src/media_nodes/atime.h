/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef TICKER_H
#define TICKER_H

#include "support/numbers.h"
#include <OS.h>


//=======================================================
// class: ATime
//
//	This is the base class for any measure of time.  The
//  base representation of time is seconds.  Thus whenever
//	one time is constructed from another, the data is 
//	converted to seconds when the base() method is called.
//
//=======================================================
class ATime
{
public:
			ATime();
			ATime(const double aValue);
			ATime(const ATime& other);
	virtual	~ATime();
	
	ATime&	operator= (const ATime& other);
	ATime&	operator- (const ATime& other);
	
	void			SetSeconds(const double secs);
	const double	GetSeconds() const;

	double	fSeconds;

protected:	
	
private:
};


//=======================================================
//
//=======================================================

class seconds : public ATime
{
public:
		seconds(const double aValue=0):ATime(aValue){};
		seconds(const ATime& other) {fSeconds = other.fSeconds;};
	
		operator double() { return fSeconds; };

protected:

private:
};

//=======================================================
//
//=======================================================
class microseconds : public ATime
{
public:
		microseconds(const double aValue=0):ATime(aValue/kmega){};
		microseconds(const ATime& other) {fSeconds = other.fSeconds;};
	
		operator double() { return fSeconds*kmega; };

protected:

private:
};


//=======================================================
// Class: AATime
//
//	A thread that provides a tick event on a timed basis
//=======================================================

// Tick rates for SMPTE time base
const double kSMPTE24fps = 24.0;
const double kSMPTE25fps = 25.0;
const double kSMPTE30fpsdrop = 29.97002617;
const double kSMPTE30fps = 30.0;


// MIDI tick rate
const double kMIDIRate = kmega;

class ATicker
{
public:
			ATicker(ATime &tickInterval);		// Interval between ticks
			ATicker(const double tickRate);		// Number of times to tick a second
	virtual	~ATicker();
	
	virtual	void	Tick(const double tickCount);
	
			void	GetInterval(ATime &interval);
			void	SetInterval(const double tickRate);

	microseconds	fInterval;
protected:
	

private:
			// Don't allow default construction?
			ATicker();
	
	thread_id	fThreadID;
};



#endif // TICKER_H
