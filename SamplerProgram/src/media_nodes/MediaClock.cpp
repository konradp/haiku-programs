#include "MediaClock.h"
#include "MainWindow.h"

#include <stdio.h>

MediaClock::MediaClock(MainWindow *mainWindow, const double tickRate)
	:	ATicker(tickRate),
		fMainWindow(mainWindow),
		fMessenger(NULL)
{
}

void
MediaClock::Tick(const double tickCount)
{
	//printf("MediaClock:: Tick() \n");
	//fMessenger->SendMessage(new BMessage(MSG_TICK));
	fMainWindow->Pulse();
}

void
MediaClock::SetInterval(const double interval)
{
	printf("MediaClock:: SetInterval %f \n", interval);
	//fInterval = interval;
	ATicker::SetInterval(interval);
}

/*void
MediaClock::		SetTarget(BHandler* handler)
{
	delete fMessenger;
	fMessenger = new BMessenger(handler);
}*/
