/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef MIDIIN_H
#define MIDIIN_H

#include <MidiConsumer.h>
#include <SupportDefs.h>

//#include "PatternEditView.h"


class MidiIn : public BMidiLocalConsumer
{
public:
	MidiIn(const char* name);
	
	virtual void	NoteOn(
					uchar channel, uchar note, uchar velocity, bigtime_t time);
	virtual void	NoteOff(
					uchar channel, uchar note, uchar velocity, bigtime_t time);						
private:

};


#endif // MIDIIN_H
