#ifndef NODEMANAGER_H
#define NODEMANAGER_H

#include <Directory.h>
#include <Entry.h>
#include <File.h>
#include <Looper.h>
#include <SupportDefs.h>

#include <MediaNode.h>
#include <MediaRoster.h>
#include <media/MediaRecorder.h>

#include <MessageRunner.h>
#include <Messenger.h>

#include "media_nodes/MonoSynth.h"

using BPrivate::media::BMediaRecorder;

class BMessageRunner;
class BMessenger;

class NodeManager : public BLooper {
public:
					NodeManager();
	virtual			~NodeManager();
	virtual void	MessageReceived(BMessage* message);
	void			_Record(BMessage * message);
	void 			SetTarget(BHandler* handler);

private:
	status_t		_SetUpNodes();
	status_t		_StartMonoSynth();
	void			_StopMonoSynth();
	status_t		_TearDownNodes();

private:
	BMessenger*		fMessenger;
	BMessageRunner*	fMessageRunner;
	struct Connection {
		media_node			producer;
		media_node			consumer;
		media_source		source;
		media_destination	destination;
		media_format		format;
		bool				connected;
	};

private:
	BMediaRoster*	fRoster;
	
	// MEDIA NODES
	media_node		fTimeSource;
	media_node		fAudioMixerNode;	//include libmedia.so for this
	media_node		fAudioInputNode;
	//media_node	producer;
	
	// MONO SYNTH
	status_t		_SetUpMonoSynth();
	MonoSynth*		fMonoSynth;
	Connection		fMonoSynthConnection;

private:
	// RECORDER methods
	status_t		_SetUpRecorder();
	
	status_t		_NewTempRecName(char * buffer);
	status_t		_MakeRecordConnection(const media_node & input);
	
	status_t		_BreakRecordConnection();
	status_t		_StopRecording();
	static	void	_RecordFile(void * cookie, bigtime_t timestamp, void * data, size_t size, const media_format & format);
	static	void	_NotifyRecordFile(void * cookie, BMediaRecorder::notification code, ...);
	// RECORDER files
	BMediaRecorder*	fRecorder;
	bool			fRecording;
	BDirectory		fTempDir;
	
	int				fTempRecCount;
	
	BEntry			fRecEntry;
	media_format	fRecordFormat;
	BFile			fRecFile;
	off_t			fRecSize;	//what is this?
	
private:	
	status_t		fStatus;	
};

#endif // NODEMANAGER_H
