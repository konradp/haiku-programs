/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#include <stdio.h>

#include <Application.h>
#include <View.h>
#include <Window.h>
#include <TypeConstants.h>
#include <SupportDefs.h>

#include "support/messages.h"
#include "MidiIn.h"

MidiIn::MidiIn(const char* name)
	:	BMidiLocalConsumer(name)
{		
}

void MidiIn::NoteOn(uchar channel, uchar note, uchar velocity, bigtime_t time)
{
	printf("MidiIn: NoteOn received \n");
	BWindow *win = be_app->WindowAt(0);
	//printf("No of windows: %li \n", be_app->CountWindows());
	if (win) {		
		BMessage msg(MSG_MIDI_NOTEON);
		msg.AddData("note", B_INT32_TYPE, &note, sizeof(note));

		win->PostMessage(&msg);
		printf("MidiIn: Message posted \n");
	}
}
void MidiIn::NoteOff(uchar channel, uchar note, uchar velocity, bigtime_t time)
{
	printf("MidiIn: NoteOff received \n");
	BWindow *win = be_app->WindowAt(0);
	if (win) {	
		BMessage msg(NOTEOFF_MSG);
		msg.AddData("note", B_INT32_TYPE, &note, sizeof(note));
		
		win->PostMessage(&msg);
		printf("MidiIn: Message posted \n");
	}
}
