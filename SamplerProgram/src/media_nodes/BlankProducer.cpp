#include "BlankProducer.h"

#include <media/BufferGroup.h>
#include <media/Buffer.h>
#include <media/MediaDefs.h>
#include <stdio.h>

#include <media/TimeSource.h>


BlankProducer::BlankProducer()
	:	BMediaNode("BlankProducer"),
		BBufferProducer(B_MEDIA_RAW_AUDIO),
		mBufferGroup(NULL)
{
	printf("BlankProducer constructor \n");
	// initialize our preferred format object
	mPreferredFormat.type = B_MEDIA_RAW_AUDIO;
	mPreferredFormat.u.raw_audio.format = media_raw_audio_format::B_AUDIO_FLOAT;
	mPreferredFormat.u.raw_audio.channel_count = 1;
	mPreferredFormat.u.raw_audio.frame_rate = 44100;		// measured in Hertz
	mPreferredFormat.u.raw_audio.byte_order = (B_HOST_IS_BENDIAN) ? B_MEDIA_BIG_ENDIAN : B_MEDIA_LITTLE_ENDIAN;

	// we'll use the consumer's preferred buffer size, if any
	mPreferredFormat.u.raw_audio.buffer_size = media_raw_audio_format::wildcard.buffer_size;

	// we're not connected yet
	mOutput.destination = media_destination::null;
	mOutput.format = mPreferredFormat;		       
}

BlankProducer::~BlankProducer()
{
	printf("Node destructor \n");
	//Quit();
}
	
//PRODUCER

status_t
BlankProducer::FormatSuggestionRequested(media_type type, int32 quality, media_format* format)
{
	printf("FormatSuggestionRequested \n");
	return B_OK;
}

status_t
BlankProducer::FormatProposal(const media_source& output, media_format* format)
{
	printf("FormatProposal \n");
	return B_OK;
}

status_t
BlankProducer::FormatChangeRequested(const media_source& source, const media_destination& destination, media_format* ioFormat, int32* _deprecated_)
{
	printf("FormatChangeRequested \n");
	return B_ERROR;
}
				
status_t
BlankProducer::GetNextOutput(int32* cookie, media_output* outOutput)
{
	if (*cookie == 0)
	{
		*outOutput = mOutput;
		*cookie += 1;
		printf("GetNextOutput. B_OK. Cookie: %lu \n", *cookie);
		return B_OK;
	}
	else
	{
		printf("GetNextOutput. B_BAD_INDEX. Cookie: %lu \n", *cookie);
		return B_BAD_INDEX;	
	}
}

status_t
BlankProducer::DisposeOutputCookie(int32 cookie)
{
	printf("DisposeOutputCookie \n");
	return B_OK;
}

status_t
BlankProducer::SetBufferGroup(const media_source& forSource, BBufferGroup* group)
{
	printf("SetBufferGroup \n");
	return B_OK;
}

status_t
BlankProducer::GetLatency(bigtime_t* outLatency)
{
	printf("GetLatency \n");
	*outLatency = 1000; //EventLatency() + SchedulingLatency();
	return B_OK;
}

status_t
BlankProducer::PrepareToConnect(const media_source& whichSource, const media_destination& whichDestination, media_format* format, media_source* outSource, char* outName)
{
	printf("PrepareToConnect \n");
	return B_OK;
}

void
BlankProducer::Connect(status_t status, const media_source& source, const media_destination& destination, const media_format& format, char* ioName)
{
	printf("Connect \n");
}

void
BlankProducer::Disconnect(const media_source& source, const media_destination& destination)
{
	printf("Disconnect \n");
}

void
BlankProducer::LateNoticeReceived(const media_source& whichSource, bigtime_t howLate, bigtime_t performanceTime)
{
	printf("LateNoticeReceived \n");
}

void
BlankProducer::EnableOutput(const media_source& whichOutput, bool enabled, int32* _deprecated_)
{
	printf("EnableOutput \n");
}

status_t
BlankProducer::SetPlayRate(int32 numerator, int32 denominator)
{
	printf("SetPlayRate \n");
	return B_OK;
}

status_t
BlankProducer::HandleMessage(int32 message, const void* data, size_t size)
{
	printf("HandleMessage \n");
	return B_OK;
}

void	//void now, not status_t
BlankProducer::AdditionalBufferRequested(const media_source& source, media_buffer_id previousBufferID, bigtime_t previousTime, const media_seek_tag* previousTag)
{
	printf("AdditionalBufferRequested \n");
	//SendBuffer();
	//return B_OK;
}

void
BlankProducer::LatencyChanged(const media_source& source, const media_destination& destination, bigtime_t newLatency, uint32 flags)
{
	printf("LatencyChanged \n");
}

BMediaAddOn*
BlankProducer::AddOn(int32* internalID) const
{
	printf("AddOn \n");
	return NULL;
}

//BMEDIAEVENTLOOPER METHODS
void
BlankProducer::NodeRegistered()
{
	printf("BlankProducer::NodeRegistered\n");
}

/*
status_t
BlankProducer::	ChangeFormat(const media_source& source, const media_destination& destination, media_format* newFormat)
static status_t
BlankProducer::	ClipDataToRegion(int32 format, int32 byteCount, const void* clipData, BRegion* region)
status_t
BlankProducer::	FindLatencyFor(const media_destination& forDestination, bigtime_t* outLatency, media_node_id* outTimeSource)
status_t
BlankProducer::	FindSeekTag(const media_destination& forDestination, bigtime_t inTargetTime, media_seek_tag* outTag, bigtime_t* outTaggedTime, uint32* outFlags = 0, uint32* inFlags = 0);
status_t
BlankProducer::	ProposeFormatChange(media_format* format, const media_destination& forDestination);
status_t
BlankProducer::	SendBuffer(BBuffer* buffer, media_destination& destination);
status_t
BlankProducer::	SendDataStatus(int32 status, media_destination& destination, bigtime_t atTime);
void
BlankProducer::	SetInitialLatency(bigtime_t initialLatency, uint32 flags);
*/
