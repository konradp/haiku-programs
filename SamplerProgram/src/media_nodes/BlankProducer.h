#ifndef BlankProducer_H
#define BlankProducer_H

#include <media/BufferProducer.h>

//#include <media/MediaEventLooper.h>
//#include <media/Controllable.h>


class BlankProducer : public BBufferProducer
{
public:
	BlankProducer();
	~BlankProducer();

protected:
//MediaNode
virtual BMediaAddOn*	AddOn(
						int32* internal_id) const;

//BufferProducer
	status_t	FormatSuggestionRequested(
			    media_type					type,
			    int32						quality,
			    media_format*				format);

	status_t	FormatProposal(
		    	const media_source&			output,
		    	media_format* 				format);

	status_t	FormatChangeRequested(
		    	const media_source& 		source,
		    	const media_destination&	destination,
		    	media_format*				ioFormat,
		    	int32* 						_deprecated_);
				
	status_t	GetNextOutput(
		    	int32*						cookie,
		    	media_output*				outOutput);

	status_t	DisposeOutputCookie(int32 cookie);

	status_t	SetBufferGroup(
			    const media_source&			forSource,
			    BBufferGroup*				group);

	status_t	GetLatency(bigtime_t* outLatency);

	status_t	PrepareToConnect(
			    const media_source&			whichSource,
			    const media_destination&	whichDestination,
			    media_format*				format,
			    media_source*				outSource,
			    char*						outName);

	void		Connect(
			    status_t					status,
			    const media_source&			source,
			    const media_destination&	destination,
			    const media_format&			format,
			    char*						ioName);

	void		Disconnect(
			    const media_source&			source,
			    const media_destination&	destination);

	void		LateNoticeReceived(const media_source& whichSource,
		    	bigtime_t					howLate,
		    	bigtime_t					performanceTime);

	void		EnableOutput(
			    const						media_source& whichOutput,
			    bool						enabled,
			    int32*						_deprecated_);

	status_t	SetPlayRate(int32 numerator, int32 denominator);

	status_t	HandleMessage(
			    int32						message,
			    const void*					data,
			    size_t						size);

	void		AdditionalBufferRequested(
			    const media_source&			source,
			    media_buffer_id				previousBufferID,
			    bigtime_t					previousTime,
			    const media_seek_tag*		previousTag);		//returns void now, not status_t
		    
	void		LatencyChanged(
				const media_source&			source,
				const media_destination&	destination,
				bigtime_t					newLatency,
				uint32						flags);
			
// BMediaEventLooper methods
	void NodeRegistered();

private:
	BBuffer*	FillNextBuffer(bigtime_t event_time);

	BBufferGroup*	mBufferGroup;
	media_output	mOutput;
	media_format	mPreferredFormat;

};

/*
  status_t	ChangeFormat(
		  const media_source& source,
		  const media_destination& destination,
		  media_format* newFormat);

  static status_t	ClipDataToRegion(
			  int32 format,
			  int32 byteCount,
			  const void* clipData,
			  BRegion* region);

  status_t		FindLatencyFor(
			  const media_destination& forDestination,
			  bigtime_t* outLatency,
			  media_node_id* outTimeSource);
			
status_t		FindSeekTag(
			  const media_destination& forDestination,
			  bigtime_t inTargetTime,
			  media_seek_tag* outTag,
			  bigtime_t* outTaggedTime,
			  uint32* outFlags = 0,
			  uint32* inFlags = 0);


				  
  status_t		ProposeFormatChange(
			  media_format* format,
			  const media_destination& forDestination);

  status_t		SendBuffer(
			  BBuffer* buffer,
			  media_destination& destination);

  status_t		SendDataStatus(
			  int32 status,
			  media_destination& destination,
			  bigtime_t atTime);

				
  void			SetInitialLatency(
			  bigtime_t initialLatency,
			  uint32 flags);
			*/
#endif
