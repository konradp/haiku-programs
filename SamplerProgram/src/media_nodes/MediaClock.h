#ifndef MEDIACLOCK_H
#define MEDIACLOCK_H

#include "atime.h"

#include <interface/Window.h>

class MainWindow;

class MediaClock : public ATicker
{
public:
			MediaClock(MainWindow *mainWindow, const double tickRate);
			
	virtual void	Tick(const double tickCount);
	void 			SetTarget(BHandler* handler);
	void 			SetInterval(const double interval);
	//int area() { return fSeconds };
	
protected:
	MainWindow	*fMainWindow;
	
private:
	BMessenger*		fMessenger;
	BMessageRunner*	fMessageRunner;

};

#endif
