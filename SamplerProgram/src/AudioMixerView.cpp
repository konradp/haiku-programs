/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#include "AudioMixerView.h"
#include "support/messages.h"

#include <MessageRunner.h>
#include <Messenger.h>
#include <stdio.h>

AudioMixerView::			AudioMixerView(BRect rect)		//Constructor
	:				BView(rect, "Settings View", B_FOLLOW_ALL, B_WILL_DRAW | B_NAVIGABLE_JUMP),
					fMessenger(NULL),
					fMessageRunner(NULL)
{
}

AudioMixerView::~AudioMixerView()
{
	delete fMessenger;
	delete fMessageRunner;
}

void
AudioMixerView::		AttachedToWindow()
{
	
}

void
AudioMixerView::Draw(BRect updateRect)
{
}

//Message Received
void
AudioMixerView::			MessageReceived(BMessage* msg)
{
	if (!fMessenger) {
		printf("fMessenger not exists \n");
		BView::MessageReceived(msg);
		return;
	}

	switch (msg->what)
	{
		case MSG_MIDI_NOTEON:
		{
			printf("AudioMixerView: Connect \n");
			fMessenger->SendMessage(msg);
			break;
		}
		default:
		{
			BView::MessageReceived(msg);
			break;
		}
	}
}

void
AudioMixerView::		SetTarget(BHandler* handler)
{
	delete fMessenger;
	fMessenger = new BMessenger(handler);
}
