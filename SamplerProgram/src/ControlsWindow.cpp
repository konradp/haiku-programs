/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#include "ControlsWindow.h"
#include "support/messages.h"


#include <ControlLook.h>
#include <stdio.h>
#include <LayoutBuilder.h>
#include <GroupLayoutBuilder.h>
#include <ListView.h>
#include <TabView.h>
#include <MessageRunner.h>
#include <Messenger.h>

#include <ScrollView.h>


ControlsWindow::ControlsWindow()
	:
	BWindow(BRect(750, 95, 950, 300), "Controls", B_FLOATING_WINDOW_LOOK, B_FLOATING_APP_WINDOW_FEEL,
		B_NOT_CLOSABLE | B_NOT_ZOOMABLE | B_NOT_RESIZABLE | B_ASYNCHRONOUS_CONTROLS)
{
	fSettingsView = new SettingsView();
	BListView* listView = new BListView("list view", B_SINGLE_SELECTION_LIST);
	listView->SetSelectionMessage(new BMessage('msge'));
	BStringItem* item = new BStringItem("Item 1");
	listView->AddItem(item);
	item = new BStringItem("Item 2");
	listView->AddItem(item);
	BScrollView* scrollView = new BScrollView("listView", listView, 0, false, true);
	
	BTabView* tabView = new BTabView("tabView", B_WIDTH_AS_USUAL);
	tabView->AddTab(fSettingsView);
	tabView->AddTab(scrollView);

	// TOP BACKGROUND VIEW
	BView* topView = new BView("topView", B_WILL_DRAW);
	BLayoutBuilder::Group<>(topView, B_VERTICAL)
		.SetInsets(0, 0, 0, 0)
		.Add(tabView)
		.End();
		
	SetLayout(new BGroupLayout(B_HORIZONTAL));
	GetLayout()->AddView(topView);
}


ControlsWindow::~ControlsWindow()
{
}


void
ControlsWindow::MessageReceived(BMessage* msg)
{
	switch (msg->what) {
		case CONNECT_MSG:
		{
			printf("ControlsWindow: Connect \n");
			fMessenger->SendMessage(msg);
			break;
		}
		case RESOLUTION_MSG:
		{
			printf("ControlsWindow: Resolution msg \n");
			int32 value;
			value = fSettingsView->GetResolution();
			//Encapsulate the new size value in the message
			BMessage msg(RESOLUTION_MSG);
			msg.AddInt32("Resolution", value);
			fMessenger->SendMessage(&msg);
			break;
		}
		case MSG_TEMPO:
		{
			printf("ControlsWindow: Tempo msg \n");
			int32 value;
			value = fSettingsView->GetTempo();
			//Encapsulate the new tempo value in the message
			BMessage msg(MSG_TEMPO);
			msg.AddInt32("Tempo", value);
			fMessenger->SendMessage(&msg);
			break;
		}
		default:
		{
			printf("SettingsView: default msg \n");
			//BWindow::MessageReceived(msg);
			break;
		
		}
	}
}


void
ControlsWindow::SetTarget(BHandler* handler)
{
	delete fMessenger;
	fMessenger = new BMessenger(handler);
}
