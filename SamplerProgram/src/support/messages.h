#ifndef MESSAGES_H
#define MESSAGES_H

#include <SupportDefs.h>
const uint32 RESOLUTION_MSG 	=	'size';
const uint32 MSG_TEMPO		 	=	'tmPo';
const uint32 CONNECT_MSG		=	'cnct';
const uint32 MSG_PLAY_OLD		=	'plOL';

const uint32 TICK_MSG			=	'tick';
const uint32 MSG_MENU_QUIT		=	'meQu';

const uint32 MSG_PLAY_PAUSE		=	'plPa';
const uint32 MSG_RECORD			=	'stRc';
const uint32 MSG_STOP_RECORDING	=	'stRc';
const uint32 MSG_STOP			=	'stop';
const uint32 MSG_SKIP_FORWARD	=	'skFo';
const uint32 MSG_SKIP_BACKWARDS	=	'skBa';

const uint32 SAMPLE_MESSAGE		=	'sbtn';
const uint32 MSG_TICK			=	'tick';
const uint32 MSG_MIDI_NOTEON	=	'ntOn';
const uint32 NOTEOFF_MSG		=	'ntOf';


const uint32 MSG_EXPAND_GROUP	=	'exGr';
const uint32 MSG_ADD_NODE		=	'addN';
const uint32 MSG_FILL			=	'fill';
const uint32 MSG_CLEAR			=	'clar';

const uint32 MENU_LIBRARY		=	'lbRy';
const uint32 MENU_AUDIO_MIXER	=	'Amxr';
const uint32 MENU_MIDI_MIXER	=	'Mmxr';

#endif // MESSAGES_H
