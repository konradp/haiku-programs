#ifndef WAVDEFS_H
#define WAVDEFS_H

#include <SupportDefs.h>

//=======================================================
// The chunk structure of a WAV file
//=======================================================

// The next converts four characters to int32 type (Four Character Code). This is used in the WAVE file header
#define FOURCC(a,b,c,d)	((((uint32)(d)) << 24) | (((uint32)(c)) << 16) \
	| (((uint32)(b)) << 8) | ((uint32)(a)))

//	WAVE header structs
//	We need to include 'RIFF' and 'WAVE' characters in the .wav file header
struct wav_riff_struct	
{
	uint32 riff_id;	// 'RIFF'
	uint32 len;		// file size
	uint32 wave_id;	// 'WAVE' file type header
};

struct wav_format_struct
{
	uint16 format_tag;	// format chunk marker
	uint16 channels;
	uint32 samples_per_sec;
	uint32 avg_bytes_per_sec;
	uint16 block_align;
	uint16 bits_per_sample;
};

//	WAVE chunks struct:	The data in the WAVE file is made up of chunks. This struct is a structure which will hold each chunk
struct wav_chunk_struct
{
	uint32 fourcc;
	uint32 len;
};

struct wav_struct
{
	struct wav_riff_struct riff;
	struct wav_chunk_struct format_chunk;
	struct wav_format_struct format;
	struct wav_chunk_struct data_chunk;
};

#endif // WAVDEFS_H
