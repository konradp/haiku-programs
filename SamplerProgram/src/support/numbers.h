#ifndef ESOTERIC_numbers_h
#define ESOTERIC_numbers_h

#include <SupportDefs.h>

//=======================================================
// Common international prefix for decimal multiples
//=======================================================

const double kmega = 1.0E6;

#endif // ESOTERIC_numbers_h
