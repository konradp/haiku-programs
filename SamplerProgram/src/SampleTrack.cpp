#include "SampleTrack.h"
#include <stdio.h>

SampleTrack::SampleTrack()
{
	// Create an empty list of points for each point in time
	for ( int i = 0; i < 64; i++ )
		fTimeList[i] = NULL;
}

SampleTrack::~SampleTrack()
{
}

//fNodeInfo*	
bool
SampleTrack::ItemAt(int i)
{
	if ( fTimeList[i] != NULL )
		return true;
	else
		return false;
}

void
SampleTrack::AddRemoveNode(int x)
{
	printf("SampleTrack: AddRemoveNode \n");
	if ( fTimeList[x] == NULL ) {
		printf("SampleTrack: Adding \n");
		NodeInfo* node;
		node = new NodeInfo();
		node->duration = 10;
		fTimeList[x] = node;
	}
	else {
		printf("SampleTrack: Removing %d \n", x);
		fTimeList[x] = NULL;	//This DOESNT FREE MEMORY?
	}
}

void
SampleTrack::AddNode(int x)
{
	printf("SampleTrack: AddNode \n");
	if ( fTimeList[x] == NULL ) {
		printf("SampleTrack: Adding \n");
		NodeInfo* node;
		node = new NodeInfo();
		node->duration = 10;
		fTimeList[x] = node;
	}
}

void
SampleTrack::Fill(int freq)
{			
	for (int i = 0; i < freq; i++) {
			printf("Passed if \n");
			AddNode(i*(64/freq));
	} //for
}

void
SampleTrack::Clear()
{
	// Is this bad? Do we need to free the memory?
	for ( int i = 0; i < 64; i++ )
		fTimeList[i] = NULL;
}

//SampleTrack::
