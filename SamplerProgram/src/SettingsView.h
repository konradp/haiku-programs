#ifndef SETTINGSVIEW_H
#define SETTINGSVIEW_H

#include <Button.h>
#include <Slider.h>

class BSlider;

class SettingsView: public BView {
public:
					SettingsView();
	virtual			~SettingsView();
	
	int32			GetResolution();
	int32			GetTempo();
	
private:	
	BSlider*		fNodeSizeSlider;
	BSlider*		fTempoSlider;
	BButton*		fConnectButton;
	BButton*		fPlayButton;
};

#endif // SETTINGSVIEW_H
