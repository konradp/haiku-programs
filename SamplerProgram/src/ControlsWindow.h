#ifndef CONTROLSWINDOW_H
#define CONTROLSWINDOW_H

#include <Window.h>
#include <DirectWindow.h>
#include "SettingsView.h"		//For control window

class BMessageRunner;
class BMessenger;

class ControlsWindow : public BWindow {
public:
					ControlsWindow();
	virtual			~ControlsWindow();
	virtual	void	MessageReceived(BMessage* message);
	void 			SetTarget(BHandler* handler);


								
private:
	SettingsView*	fSettingsView;

	BMessenger*		fMessenger;
	BMessageRunner*	fMessageRunner;

};


#endif // _H
