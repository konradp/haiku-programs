/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#include "MidiMixerView.h"
#include "support/messages.h"

#include <GroupLayoutBuilder.h>
#include <LayoutBuilder.h>

#include <MessageRunner.h>
#include <Messenger.h>
#include <stdio.h>

#include <Button.h>
#include <Slider.h>

MidiMixerView::		MidiMixerView()		//Constructor
	:				BView("Settings View", B_FOLLOW_ALL),
					fMessenger(NULL),
					fMessageRunner(NULL)
{
	printf("MidiMixerView: Constructor \n");
	BButton* button = new BButton(NULL, "name", "blaa", new BMessage('blaa'));
	BSlider* slider = new BSlider(NULL, "Fontsize", "Size: 8", NULL, 1, 6, B_VERTICAL);
	BSlider* slider2 = new BSlider(NULL, "Fontsize", "Size: 8", NULL, 1, 6, B_VERTICAL);
	BSlider* slider3 = new BSlider(NULL, "Fontsize", "Size: 8", NULL, 1, 6, B_VERTICAL);

	BLayoutBuilder::Group<>(this, B_HORIZONTAL)
		.SetInsets(0, 0, 0, 0)
		.Add(button)
		.Add(slider)
		.Add(slider2)
		.Add(slider3)
		.End();
}

MidiMixerView::~MidiMixerView()
{
	delete fMessenger;
	delete fMessageRunner;
}

void
MidiMixerView::		AttachedToWindow()
{
	
}

void
MidiMixerView::Draw(BRect updateRect)
{
}

//Message Received
void
MidiMixerView::			MessageReceived(BMessage* msg)
{
	if (!fMessenger) {
		printf("fMessenger not exists \n");
		BView::MessageReceived(msg);
		return;
	}

	switch (msg->what)
	{
		case MSG_MIDI_NOTEON:
		{
			printf("MidiMixerView: Connect \n");
			fMessenger->SendMessage(msg);
			break;
		}
		default:
		{
			BView::MessageReceived(msg);
			break;
		}
	}
}

void
MidiMixerView::		SetTarget(BHandler* handler)
{
	delete fMessenger;
	fMessenger = new BMessenger(handler);
}
