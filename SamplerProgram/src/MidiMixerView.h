/*
 * Copyright 2014 Your Name <your@email.address>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef MIDIMIXERVIEW_H
#define MIDIMIXERVIEW_H

#include <View.h>

class BMessageRunner;
class BMessenger;

class MidiMixerView : public BView {
public:
					MidiMixerView();
	virtual			~MidiMixerView();
	
	virtual void	AttachedToWindow();								
	virtual void	Draw(BRect updateRect);
	virtual void	MessageReceived(BMessage* message);
	void 			SetTarget(BHandler* handler);
								
private:
	BMessenger*		fMessenger;
	BMessageRunner*	fMessageRunner;

};


#endif // _H
