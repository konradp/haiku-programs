#ifndef SAMPLETRACK_H
#define SAMPLETRACK_H

#include <List.h>

struct	NodeInfo{
			int duration;	};	//is this needed?

class SampleTrack {
public:
			SampleTrack();
			~SampleTrack();
			
	//fNodeInfo*	ItemAt(int i);
	bool		ItemAt(int i);
			
	void		AddRemoveNode(int x);
	void		AddNode(int x);
	void		Fill(int freq);
	void		Clear();
	
	
				
private:
	NodeInfo*	fTimeList[64];

};


#endif // GROUPMATRIX_H
